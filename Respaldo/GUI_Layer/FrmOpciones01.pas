unit FrmOpciones01;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,
  System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RzTabs, Vcl.Buttons,
  Vcl.ExtCtrls, RzPanel, uDL_Skeleton, Vcl.ComCtrls, Vcl.Mask, RxToolEdit,
  RxCurrEdit, Vcl.Samples.Spin, Vcl.Printers, Vcl.Styles, vcl.Themes,
  Vcl.WinXCtrls, FileCtrl;

type
  TFormOpciones01 = class(TForm)
    pnlTitulo: TRzPanel;
    pnlAccion: TRzPanel;
    pnlOpciones: TRzPanel;
    btnAceptar: TBitBtn;
    btnCancelar: TBitBtn;
    pgcCampos: TRzPageControl;
    TabSheet1: TRzTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    chkRedondeoDecimales: TCheckBox;
    chkRedondeoFactor5: TCheckBox;
    cbImpresoraRecibos: TComboBoxEx;
    cbImpresoraComandas: TComboBoxEx;
    Label3: TLabel;
    edtImpuestoServicio: TCurrencyEdit;
    chkCodigoBarrasDirecto: TCheckBox;
    Label4: TLabel;
    speDiasVencimiento: TSpinEdit;
    chkAplicarImpuestoServicio: TCheckBox;
    TabSheet2: TRzTabSheet;
    Label5: TLabel;
    cbImpresora1: TComboBoxEx;
    Label6: TLabel;
    cbImpresora2: TComboBoxEx;
    Label7: TLabel;
    cbImpresora3: TComboBoxEx;
    Label8: TLabel;
    cbImpresora4: TComboBoxEx;
    chkImpuestoIncluido: TCheckBox;
    Label9: TLabel;
    GroupBox1: TGroupBox;
    rbWindows: TRadioButton;
    rbClasico: TRadioButton;
    chkSeguridad: TCheckBox;
    TabSheet3: TRzTabSheet;
    chkUsarContabilidad: TCheckBox;
    Label10: TLabel;
    edtDirectorioRespaldos: TSearchBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtDirectorioRespaldosInvokeSearch(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormOpciones01: TFormOpciones01;

  procedure InicializarForma;
  procedure FinalizarForma;
  procedure KeyDownForma(var Key: Word; Shift: TShiftState);
  procedure LimpiarCampos;
  procedure Aceptar;
  procedure Cancelar;
  procedure Modificar_Data;
  procedure LlenarImpresoras;
  procedure BuscarDirectorioRespaldos;

implementation

uses
  uSistema, ufunciones, uDL_TB_SISTEMA, BS_DBConexion, DL_DBGeneral,
  uConfig;

{$R *.dfm}

var
  Forma01: TFormOpciones01;

{$REGION 'Funciones Forma'}
procedure TFormOpciones01.FormShow(Sender: TObject);
begin
  Forma01 := FormOpciones01;

  InicializarForma;
end;

procedure TFormOpciones01.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FinalizarForma;
end;

procedure TFormOpciones01.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyDownForma(Key, Shift);
end;

procedure TFormOpciones01.btnAceptarClick(Sender: TObject);
begin
  Aceptar;
end;

procedure TFormOpciones01.btnCancelarClick(Sender: TObject);
begin
  Cancelar;
end;

procedure TFormOpciones01.edtDirectorioRespaldosInvokeSearch(Sender: TObject);
begin
  BuscarDirectorioRespaldos;
end;
{$ENDREGION}

{$REGION 'Funciones Generales'}
procedure InicializarForma;
begin
  with Forma01 do
  begin

    try
      Tag := 0;

      LlenarImpresoras;

      LimpiarCampos;
    except
      if _Resultado = -1 then
      begin
        Application.MessageBox(PChar('Ha ocurrido un error' + #13+#10 + _ErrorM),
          'Error', MB_ICONERROR);
        PostMessage(Handle, WM_CLOSE, 0, 0);
      end;
      _Resultado := -1;
    end;

  end;
end;

procedure FinalizarForma;
begin
  with Forma01 do
  begin

    try

    except
    end;

  end;
end;

procedure KeyDownForma(var Key: Word; Shift: TShiftState);
begin
  with Forma01 do
  begin

    if Key = VK_ESCAPE then
    begin
      Key := 0;
      Close;
    end;
    if Key = VK_F10 then
    begin
      Key := 0;
      btnAceptar.Click;
    end;
    if Key = VK_F11 then
    begin
      Key := 0;
      btnCancelar.Click;
    end;

  end;
end;

procedure LimpiarCampos;
var
  mDL_TB_SISTEMA: TDL_TB_SISTEMA;
  mConfig: TConfig;
begin
  with Forma01 do
  begin

    pgcCampos.ActivePageIndex := 0;
    cbImpresoraRecibos.ItemIndex := 0;
    cbImpresoraComandas.ItemIndex := 0;
    chkRedondeoDecimales.Checked := False;
    chkRedondeoFactor5.Checked := False;
    edtImpuestoServicio.Value := 0;
    chkSeguridad.Checked := False;
    chkUsarContabilidad.Checked := False;

    mDL_TB_SISTEMA := TDL_TB_SISTEMA.Create;
    mDL_TB_SISTEMA.Consultar(_Resultado, _ErrorM);
    with mDL_TB_SISTEMA.Dataset do
    begin
      if IsEmpty = False then
      begin
        edtImpuestoServicio.Value :=
          FieldByName('ISERVICIO').AsFloat;
        if FieldByName('APLICARREDONDEO').AsInteger = 1 then
          chkRedondeoDecimales.Checked := True;
        if FieldByName('APLICARREDONDEO5').AsInteger = 1 then
          chkRedondeoFactor5.Checked := True;
        if FieldByName('PRODUCTOIMPUESTOINCLUIDO').AsInteger = 1 then
          chkImpuestoIncluido.Checked := True
        else
          chkImpuestoIncluido.Checked := False;
        if FieldByName('USARSEGURIDAD').AsInteger = 1 then
          chkSeguridad.Checked := True;
        if FieldByName('USARCONTABILIDAD').AsInteger = 1 then
          chkUsarContabilidad.Checked := True;
      end;
    end;
    mDL_TB_SISTEMA.Destroy;

    mConfig := TConfig.Create;
    mConfig.LeerIni;
    mConfig.Destroy;
    if Global.AplicarImpuestoServicio = 1 then
      chkAplicarImpuestoServicio.Checked := True
    else
      chkAplicarImpuestoServicio.Checked := False;
    if Global.CodigoBarrasDirecto = 1 then
      chkCodigoBarrasDirecto.Checked := True
    else
      chkCodigoBarrasDirecto.Checked := False;
    speDiasVencimiento.Value :=
      Global.DiasVencimientoDefault;
    cbImpresoraRecibos.ItemIndex := cbImpresoraRecibos.Items.IndexOf(Global.ImpresoraRecibos);
    cbImpresoraComandas.ItemIndex := cbImpresoraComandas.Items.IndexOf(Global.ImpresoraComanda);
    cbImpresora1.ItemIndex := cbImpresora1.Items.IndexOf(Global.OrdenesImpresora1);
    cbImpresora2.ItemIndex := cbImpresora2.Items.IndexOf(Global.OrdenesImpresora2);
    cbImpresora3.ItemIndex := cbImpresora3.Items.IndexOf(Global.OrdenesImpresora3);
    cbImpresora4.ItemIndex := cbImpresora4.Items.IndexOf(Global.OrdenesImpresora4);
    if Global.TipoPantalla = 0 then
      rbWindows.Checked := True;
    if Global.TipoPantalla = 1 then
      rbClasico.Checked := True;
    edtDirectorioRespaldos.Text := Global.DirectorioRespaldos;

  end;
end;

procedure Aceptar;
begin
  with Forma01 do
  begin

    Modificar_Data;
    if _Resultado = 1 then
    begin
      Tag := 1;
      Close;
    end;

  end;
end;

procedure Cancelar;
begin
  with Forma01 do
  begin

    Close;

  end;
end;

procedure Modificar_Data;
var
  mDL_TB_SISTEMA: TDL_TB_SISTEMA;
  mConfig: TConfig;
  mWhere: TStringList;
  mResultado: Integer;
  mErrorM: string;
begin
  with Forma01 do
  begin

    try
      _Resultado := 1;

      _MainConexion.Iniciar_Transaccion(_Resultado, _ErrorM);

      mDL_TB_SISTEMA := TDL_TB_SISTEMA.Create;
      with mDL_TB_SISTEMA.Dataset do
      begin
        EmptyDataSet;
        mDL_TB_SISTEMA.Limpiar_Tag;
        Append;
        FieldByName('ISERVICIO').Tag := 1;
        FieldByName('ISERVICIO').AsFloat :=
          edtImpuestoServicio.Value;
        FieldByName('APLICARREDONDEO').Tag := 1;
        if chkRedondeoDecimales.Checked = True then
          FieldByName('APLICARREDONDEO').AsInteger := 1
        else
          FieldByName('APLICARREDONDEO').AsInteger := 0;
        FieldByName('APLICARREDONDEO5').Tag := 1;
        if chkRedondeoFactor5.Checked = True then
          FieldByName('APLICARREDONDEO5').AsInteger := 1
        else
          FieldByName('APLICARREDONDEO5').AsInteger := 0;
        FieldByName('PRODUCTOIMPUESTOINCLUIDO').Tag := 1;
        if chkImpuestoIncluido.Checked = True then
          FieldByName('PRODUCTOIMPUESTOINCLUIDO').AsInteger := 1
        else
          FieldByName('PRODUCTOIMPUESTOINCLUIDO').AsInteger := 0;
        FieldByName('USARSEGURIDAD').Tag := 1;
        if chkSeguridad.Checked = True then
          FieldByName('USARSEGURIDAD').AsInteger := 1
        else
          FieldByName('USARSEGURIDAD').AsInteger := 0;
        FieldByName('USARCONTABILIDAD').Tag := 1;
        if chkUsarContabilidad.Checked = True then
          FieldByName('USARCONTABILIDAD').AsInteger := 1
        else
          FieldByName('USARCONTABILIDAD').AsInteger := 0;
        Post;
      end;
      mDL_TB_SISTEMA.Modificar('', _Resultado, _ErrorM);
      if _Resultado = -1 then
        raise Exception.Create('');
      mDL_TB_SISTEMA.Destroy;

      if chkAplicarImpuestoServicio.Checked = True then
        Global.AplicarImpuestoServicio := 1
      else
        Global.AplicarImpuestoServicio := 0;
      Global.ImpresoraComanda := Trim(cbImpresoraComandas.Text);
      Global.ImpresoraRecibos := Trim(cbImpresoraRecibos.Text);
      if chkCodigoBarrasDirecto.Checked = True then
        Global.CodigoBarrasDirecto := 1
      else
        Global.CodigoBarrasDirecto := 0;
      Global.DiasVencimientoDefault :=
        speDiasVencimiento.Value;
      Global.OrdenesImpresora1 := Trim(cbImpresora1.Text);
      Global.OrdenesImpresora2 := Trim(cbImpresora2.Text);
      Global.OrdenesImpresora3 := Trim(cbImpresora3.Text);
      Global.OrdenesImpresora4 := Trim(cbImpresora4.Text);
      if rbWindows.Checked = True then
        Global.TipoPantalla := 0;
      if rbClasico.Checked = True then
        Global.TipoPantalla := 1;
      Global.DirectorioRespaldos :=
        edtDirectorioRespaldos.Text;

      mConfig := TConfig.Create;
      mConfig.EscribirIni_Sistema;
      mConfig.Destroy;

      case Global.TipoPantalla of
         0: TStyleManager.SetStyle('Windows');
         1: TStyleManager.SetStyle('Slate Classico');
      else
        TStyleManager.SetStyle('Windows');
      end;

      _MainConexion.Aceptar_Transaccion(_Resultado, _ErrorM);

      _Resultado := 1;
    except
      mResultado := _Resultado;
      mErrorM := _ErrorM;
      _MainConexion.Rechazar_Transaccion(_Resultado, _ErrorM);
      _Resultado := mResultado;
      _ErrorM := mErrorM;
      if _Resultado = -1 then
      begin
        Application.MessageBox(PChar('Ha ocurrido un error' + #13+#10 + _ErrorM),
          'Error', MB_ICONERROR);
      end;
      if _Resultado = 0 then
      begin
        Application.MessageBox(PChar(_ErrorM),
          'Advertencia', MB_ICONWARNING);
      end;
      _Resultado := -1;
    end;

  end;
end;

procedure LlenarImpresoras;
var
  mFila: Integer;
begin
  with Forma01 do
  begin

    cbImpresoraRecibos.Items.Clear;
    cbImpresoraComandas.Items.Clear;
    cbImpresora1.Items.Clear;
    cbImpresora2.Items.Clear;
    cbImpresora3.Items.Clear;
    cbImpresora4.Items.Clear;

    cbImpresoraRecibos.Items.Add('Default');
    cbImpresoraComandas.Items.Add('Default');
    cbImpresora1.Items.Add('Default');
    cbImpresora2.Items.Add('Default');
    cbImpresora3.Items.Add('Default');
    cbImpresora4.Items.Add('Default');

    for mFila := 0 to Printer.Printers.Count - 1 do
    begin
      cbImpresoraRecibos.Items.Add(Printer.Printers.strings[mFila]);
      cbImpresoraComandas.Items.Add(Printer.Printers.strings[mFila]);
      cbImpresora1.Items.Add(Printer.Printers.strings[mFila]);
      cbImpresora2.Items.Add(Printer.Printers.strings[mFila]);
      cbImpresora3.Items.Add(Printer.Printers.strings[mFila]);
      cbImpresora4.Items.Add(Printer.Printers.strings[mFila]);
    end;

  end;
end;

procedure BuscarDirectorioRespaldos;
var
  chosenDirectory: string;
begin
  with Forma01 do
  begin

    if selectdirectory('Seleccione un Directorio',
      edtDirectorioRespaldos.Text, chosenDirectory) then
      edtDirectorioRespaldos.Text := chosenDirectory;

  end;
end;
{$ENDREGION}

end.
