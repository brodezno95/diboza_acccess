unit FrmPrincipal01;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,
  System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzTabs, RzPanel, Vcl.Menus,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.FileCtrl, RzSpnEdt, Vcl.Imaging.jpeg,
  Winapi.ShellAPI, Vcl.Styles, vcl.Themes;

type
  TFormPrincipal01 = class(TForm)
    MainMenu1: TMainMenu;
    Sistema1: TMenuItem;
    RzPanel1: TRzPanel;
    pnlCampos: TRzPanel;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    TabSheet3: TRzTabSheet;
    BasedeDatos1: TMenuItem;
    Empresas1: TMenuItem;
    OpenDialog1: TOpenDialog;
    Usuarios1: TMenuItem;
    Opciones1: TMenuItem;
    N1: TMenuItem;
    Salir1: TMenuItem;
    Mantenimientos1: TMenuItem;
    Familias1: TMenuItem;
    Productos1: TMenuItem;
    Clientes1: TMenuItem;
    Proveedores1: TMenuItem;
    Vendedores1: TMenuItem;
    Impuestos1: TMenuItem;
    RzRapidFireButton1: TRzRapidFireButton;
    N2: TMenuItem;
    CambiarPrecios1: TMenuItem;
    Facturacin1: TMenuItem;
    FacturaGeneral1: TMenuItem;
    Image1: TImage;
    Plantillas1: TMenuItem;
    Ayuda1: TMenuItem;
    Acercade1: TMenuItem;
    Proformas1: TMenuItem;
    Apartados1: TMenuItem;
    Compras1: TMenuItem;
    Entradas1: TMenuItem;
    Recibos1: TMenuItem;
    Recibos2: TMenuItem;
    CierresdeCaja1: TMenuItem;
    Reportes1: TMenuItem;
    Manual1: TMenuItem;
    N3: TMenuItem;
    RzRapidFireButton2: TRzRapidFireButton;
    RzRapidFireButton3: TRzRapidFireButton;
    RzRapidFireButton4: TRzRapidFireButton;
    RzRapidFireButton5: TRzRapidFireButton;
    RzRapidFireButton6: TRzRapidFireButton;
    RzRapidFireButton7: TRzRapidFireButton;
    RzRapidFireButton8: TRzRapidFireButton;
    RzRapidFireButton9: TRzRapidFireButton;
    RzRapidFireButton10: TRzRapidFireButton;
    Image2: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    ListadodeFamilias1: TMenuItem;
    ListadodeProductos1: TMenuItem;
    ListadodeClientes1: TMenuItem;
    ListadodeProveedores1: TMenuItem;
    ListadodeVendedores1: TMenuItem;
    Entradas2: TMenuItem;
    Facturas1: TMenuItem;
    RecibosEntradas1: TMenuItem;
    RecibosFacturas1: TMenuItem;
    RecibosApartados1: TMenuItem;
    Apartados2: TMenuItem;
    Proformas2: TMenuItem;
    CierresdeCaja2: TMenuItem;
    Compras2: TMenuItem;
    Ventas1: TMenuItem;
    EstadodeCuenta1: TMenuItem;
    Grupos1: TMenuItem;
    NotasdeCrdito1: TMenuItem;
    Utilidad1: TMenuItem;
    ValordelInventario1: TMenuItem;
    RecibosdeApartados1: TMenuItem;
    Ordenes1: TMenuItem;
    lblLicencia: TLabel;
    Licencia1: TMenuItem;
    lvlVersion: TLabel;
    Contabilidad1: TMenuItem;
    Cuentas1: TMenuItem;
    Estadistico1: TMenuItem;
    Asientos1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Empresas1Click(Sender: TObject);
    procedure BasedeDatos1Click(Sender: TObject);
    procedure Usuarios1Click(Sender: TObject);
    procedure Opciones1Click(Sender: TObject);
    procedure Salir1Click(Sender: TObject);
    procedure Familias1Click(Sender: TObject);
    procedure Productos1Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Proveedores1Click(Sender: TObject);
    procedure Vendedores1Click(Sender: TObject);
    procedure Impuestos1Click(Sender: TObject);
    procedure CambiarPrecios1Click(Sender: TObject);
    procedure FacturaGeneral1Click(Sender: TObject);
    procedure Plantillas1Click(Sender: TObject);
    procedure Acercade1Click(Sender: TObject);
    procedure Proformas1Click(Sender: TObject);
    procedure Apartados1Click(Sender: TObject);
    procedure Entradas1Click(Sender: TObject);
    procedure RzRapidFireButton4Click(Sender: TObject);
    procedure RzRapidFireButton3Click(Sender: TObject);
    procedure RzRapidFireButton2Click(Sender: TObject);
    procedure RzRapidFireButton1Click(Sender: TObject);
    procedure RzRapidFireButton5Click(Sender: TObject);
    procedure RzRapidFireButton7Click(Sender: TObject);
    procedure RzRapidFireButton8Click(Sender: TObject);
    procedure RzRapidFireButton10Click(Sender: TObject);
    procedure Recibos1Click(Sender: TObject);
    procedure Recibos2Click(Sender: TObject);
    procedure CierresdeCaja1Click(Sender: TObject);
    procedure RzRapidFireButton6Click(Sender: TObject);
    procedure RzRapidFireButton9Click(Sender: TObject);
    procedure ListadodeFamilias1Click(Sender: TObject);
    procedure ListadodeProductos1Click(Sender: TObject);
    procedure ListadodeClientes1Click(Sender: TObject);
    procedure ListadodeProveedores1Click(Sender: TObject);
    procedure ListadodeVendedores1Click(Sender: TObject);
    procedure Entradas2Click(Sender: TObject);
    procedure Facturas1Click(Sender: TObject);
    procedure RecibosEntradas1Click(Sender: TObject);
    procedure RecibosFacturas1Click(Sender: TObject);
    procedure RecibosApartados1Click(Sender: TObject);
    procedure Apartados2Click(Sender: TObject);
    procedure Proformas2Click(Sender: TObject);
    procedure CierresdeCaja2Click(Sender: TObject);
    procedure Compras2Click(Sender: TObject);
    procedure Ventas1Click(Sender: TObject);
    procedure EstadodeCuenta1Click(Sender: TObject);
    procedure Grupos1Click(Sender: TObject);
    procedure NotasdeCrdito1Click(Sender: TObject);
    procedure Utilidad1Click(Sender: TObject);
    procedure ValordelInventario1Click(Sender: TObject);
    procedure RecibosdeApartados1Click(Sender: TObject);
    procedure Ordenes1Click(Sender: TObject);
    procedure Manual1Click(Sender: TObject);
    procedure Licencia1Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Cuentas1Click(Sender: TObject);
    procedure Estadistico1Click(Sender: TObject);
    procedure Asientos1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPrincipal01: TFormPrincipal01;

  procedure InicializarForma;
  procedure FinalizarForma;
  procedure KeyDownForma(var Key: Word; Shift: TShiftState);
  {$REGION 'Funciones Menu'}
  procedure mmBaseDatos;
  procedure mmEmpresas;
  procedure mmUsuarios;
  procedure mmOpciones;
  procedure mmPlantillas;
  procedure mmSalir;
  procedure mmFamilias;
  procedure mmGrupos;
  procedure mmProductos;
  procedure mmClientes;
  procedure mmProveedores;
  procedure mmVendedores;
  procedure mmImpuestos;
  procedure mmCambiarPrecios;
  procedure mmCompras;
  procedure mmRecibosProveedores;
  procedure mmFacturacion;
  procedure mmProformas;
  procedure mmApartados;
  procedure mmRecibosClientes;
  procedure mmCierresCaja;
  procedure mmRecibosApartados;
  procedure mmManual;
  procedure mmAcercaDe;
  procedure mmLicencia;
  procedure mmFacturas_Nuevos;
  procedure mmProformas_Nuevos;
  procedure mmEntradas_Nuevos;
  procedure mmApartados_Nuevos;
  procedure mmRecibosClientes_Nuevos;
  procedure mmCierresCaja_Nuevos;
  procedure mmOrdenes;
  procedure mmCuentas;
  procedure mmAsientos;
  procedure mmListadoFamlias;
  procedure mmListadoProductos;
  procedure mmListadoClientes;
  procedure mmListadoProveedores;
  procedure mmListadoVendedores;
  procedure mmFacturasProveedores;
  procedure mmRecibosAbonosProveedores;
  procedure mmFacturasClientes;
  procedure mmRecibosAbonosClientes;
  procedure mmReporteApartados;
  procedure mmRecibosAbonosApartados;
  procedure mmReporteProformas;
  procedure mmReporteCierresCaja;
  procedure mmReporteCompras;
  procedure mmReporteVentas;
  procedure mmEstadoCuenta;
  procedure mmReporteNotasCredito;
  procedure mmUtilidad;
  procedure mmValorInventario;
  procedure mEstadistico;
  {$ENDREGION}
  function LicenciaValida(var pMensaje: string): Boolean;

implementation

uses
  uSistema, ufunciones, uConfig, BS_DBConexion, FrmPresentacion01,
  FmrEmpresas01, FrmMantenimientoUsuarios01, FrmBaseDatos01,
  FrmMantenimientoFamilias01, FrmMantenimientoProductos01,
  FrmMantenimientoClientes01, FrmMantenimientoProveedores01,
  FrmMantenimientoVendedores01, FrmMantenimientoImpuestos01,
  FrmCambiarPrecios01, FrmMantenimientoFacturasClientes01,
  FrmMantenimientoPlantillas01, FrmAcercaDe01, FrmMantenimientoProformas01,
  FrmMantenimientoApartados01, FrmMantenimientoFacturasProveedores01,
  FrmFacturaClientes01, FrmProformas01, FrmFacturasProveedores01,
  FrmApartados01, FrmOpciones01, FrmMantenimientoRecibosClientes01,
  FrmMantenimientoRecibosProveedores01, FrmMantenimientoRecibosCierresCaja01,
  FrmAcceso01, FrmMantenimientoRecibosApartados01, uDL_TB_SISTEMA,
  FrmRecibosClientes01, FrmCierresCaja01, RptListadoFamilias01,
  RptListadoProductos01, RptListadoClientes01, RptListadoProveedores01,
  RptListadoVendedores01, RptFacturasProveedores01,
  RptFacturasClientes01, RptRecibosProveedores01, RptRecibosClientes01,
  RptRecibosApartados01, RptApartados01, RptProformas01,
  RptCierresCaja01, RptCompras01, RptVentas01, RptEstadoCuenta01,
  FrmMantenimientoGrupos01, RptNotasCredito01, RptUtilidad01,
  RptValorInventario01, FrmOpcionesIniciales01, FrmMantenimientoOrdenes01,
  uLicencia, FrmPedirLicencia01, FrmActivarLicencia01,
  FrmMantenimientoCuentas01, FrmAcceso02, RptEstadistico01,
  FrmMantenimientoAsientos01;

{$R *.dfm}

var
  Forma01: TFormPrincipal01;
  _Actualizando: Boolean;
  _PasaForma: Boolean;

{$REGION 'Funciones Forma'}
procedure TFormPrincipal01.FormCreate(Sender: TObject);
var
  mConfig: TConfig;
begin
  _PasaForma := False;

  lblLicencia.Caption  := '';
  lblLicencia.Caption  := '';

  InicialiarSistema;

  mConfig := TConfig.Create;
  mConfig.LeerIni;
  mConfig.Destroy;

  case Global.TipoPantalla of
     0: TStyleManager.SetStyle('Windows');
     1: TStyleManager.SetStyle('Slate Classico');
  else
    TStyleManager.SetStyle('Windows');
  end;

  Forma01 := FormPrincipal01;
end;

procedure TFormPrincipal01.FormActivate(Sender: TObject);
begin
  if _PasaForma = False then
    InicializarForma;
  _PasaForma := True;
end;

procedure TFormPrincipal01.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FinalizarForma;
end;

procedure TFormPrincipal01.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyDownForma(Key, Shift);
end;

{$REGION 'Funciones Menu'}
procedure TFormPrincipal01.BasedeDatos1Click(Sender: TObject);
begin
  mmBaseDatos;
end;

procedure TFormPrincipal01.Empresas1Click(Sender: TObject);
begin
  mmEmpresas;
end;

procedure TFormPrincipal01.Usuarios1Click(Sender: TObject);
begin
  mmUsuarios;
end;

procedure TFormPrincipal01.Opciones1Click(Sender: TObject);
begin
  mmOpciones;
end;

procedure TFormPrincipal01.Plantillas1Click(Sender: TObject);
begin
  mmPlantillas;
end;

procedure TFormPrincipal01.Salir1Click(Sender: TObject);
begin
  mmSalir;
end;

procedure TFormPrincipal01.Familias1Click(Sender: TObject);
begin
  mmFamilias;
end;

procedure TFormPrincipal01.Grupos1Click(Sender: TObject);
begin
  mmGrupos;
end;

procedure TFormPrincipal01.Productos1Click(Sender: TObject);
begin
  mmProductos;
end;

procedure TFormPrincipal01.Clientes1Click(Sender: TObject);
begin
  mmClientes;
end;

procedure TFormPrincipal01.Proveedores1Click(Sender: TObject);
begin
  mmProveedores;
end;

procedure TFormPrincipal01.Vendedores1Click(Sender: TObject);
begin
  mmVendedores;
end;

procedure TFormPrincipal01.Impuestos1Click(Sender: TObject);
begin
  mmImpuestos;
end;

procedure TFormPrincipal01.CambiarPrecios1Click(Sender: TObject);
begin
  mmCambiarPrecios;
end;

procedure TFormPrincipal01.Entradas1Click(Sender: TObject);
begin
  mmCompras;
end;

procedure TFormPrincipal01.FacturaGeneral1Click(Sender: TObject);
begin
  mmFacturacion;
end;

procedure TFormPrincipal01.Proformas1Click(Sender: TObject);
begin
  mmProformas;
end;

procedure TFormPrincipal01.Apartados1Click(Sender: TObject);
begin
  mmApartados;
end;

procedure TFormPrincipal01.Acercade1Click(Sender: TObject);
begin
  mmAcercaDe;
end;

procedure TFormPrincipal01.RzRapidFireButton4Click(Sender: TObject);
begin
  mmProductos;
end;

procedure TFormPrincipal01.RzRapidFireButton3Click(Sender: TObject);
begin
  mmClientes;
end;

procedure TFormPrincipal01.RzRapidFireButton2Click(Sender: TObject);
begin
  mmProveedores;
end;

procedure TFormPrincipal01.RzRapidFireButton1Click(Sender: TObject);
begin
  mmCambiarPrecios;
end;

procedure TFormPrincipal01.Cuentas1Click(Sender: TObject);
begin
  mmCuentas;
end;

procedure TFormPrincipal01.Asientos1Click(Sender: TObject);
begin
  mmAsientos;
end;

procedure TFormPrincipal01.Recibos1Click(Sender: TObject);
begin
  mmRecibosClientes;
end;

procedure TFormPrincipal01.Recibos2Click(Sender: TObject);
begin
  mmRecibosProveedores;
end;

procedure TFormPrincipal01.CierresdeCaja1Click(Sender: TObject);
begin
  mmCierresCaja;
end;

procedure TFormPrincipal01.Ordenes1Click(Sender: TObject);
begin
  mmOrdenes;
end;

procedure TFormPrincipal01.RzRapidFireButton5Click(Sender: TObject);
begin
  mmFacturas_Nuevos;
end;

procedure TFormPrincipal01.RzRapidFireButton6Click(Sender: TObject);
begin
  mmRecibosClientes_Nuevos;
end;

procedure TFormPrincipal01.RzRapidFireButton7Click(Sender: TObject);
begin
  mmProformas_Nuevos;
end;

procedure TFormPrincipal01.RzRapidFireButton8Click(Sender: TObject);
begin
  mmEntradas_Nuevos;
end;

procedure TFormPrincipal01.RzRapidFireButton10Click(Sender: TObject);
begin
  mmApartados_Nuevos;
end;

procedure TFormPrincipal01.RzRapidFireButton9Click(Sender: TObject);
begin
  FormAcceso01.ShowModal;
  if FormAcceso01.Tag = 1 then
    mmCierresCaja_Nuevos;
end;

procedure TFormPrincipal01.ListadodeFamilias1Click(Sender: TObject);
begin
  mmListadoFamlias;
end;

procedure TFormPrincipal01.ListadodeProductos1Click(Sender: TObject);
begin
  mmListadoProductos;
end;

procedure TFormPrincipal01.Licencia1Click(Sender: TObject);
begin
  mmLicencia;
end;

procedure TFormPrincipal01.ListadodeClientes1Click(Sender: TObject);
begin
  mmListadoClientes;
end;

procedure TFormPrincipal01.ListadodeProveedores1Click(Sender: TObject);
begin
  mmListadoProveedores;
end;

procedure TFormPrincipal01.ListadodeVendedores1Click(Sender: TObject);
begin
  mmListadoVendedores;
end;

procedure TFormPrincipal01.Entradas2Click(Sender: TObject);
begin
  mmFacturasProveedores;
end;

procedure TFormPrincipal01.RecibosEntradas1Click(Sender: TObject);
begin
  mmRecibosAbonosProveedores
end;

procedure TFormPrincipal01.Facturas1Click(Sender: TObject);
begin
  mmFacturasClientes;
end;

procedure TFormPrincipal01.RecibosFacturas1Click(Sender: TObject);
begin
  mmRecibosAbonosClientes;
end;

procedure TFormPrincipal01.Apartados2Click(Sender: TObject);
begin
  mmReporteApartados;
end;

procedure TFormPrincipal01.RecibosApartados1Click(Sender: TObject);
begin
  mmRecibosAbonosApartados;
end;

procedure TFormPrincipal01.RecibosdeApartados1Click(Sender: TObject);
begin
  mmRecibosApartados;
end;

procedure TFormPrincipal01.Manual1Click(Sender: TObject);
begin
  mmManual;
end;

procedure TFormPrincipal01.Proformas2Click(Sender: TObject);
begin
  mmReporteProformas;
end;

procedure TFormPrincipal01.CierresdeCaja2Click(Sender: TObject);
begin
  mmReporteCierresCaja;
end;

procedure TFormPrincipal01.Compras2Click(Sender: TObject);
begin
  mmReporteCompras;
end;

procedure TFormPrincipal01.Ventas1Click(Sender: TObject);
begin
  mmReporteVentas;
end;

procedure TFormPrincipal01.EstadodeCuenta1Click(Sender: TObject);
begin
  mmEstadoCuenta;
end;

procedure TFormPrincipal01.NotasdeCrdito1Click(Sender: TObject);
begin
  mmReporteNotasCredito;
end;

procedure TFormPrincipal01.Utilidad1Click(Sender: TObject);
begin
  mmUtilidad;
end;

procedure TFormPrincipal01.ValordelInventario1Click(Sender: TObject);
begin
  mmValorInventario;
end;

procedure TFormPrincipal01.Estadistico1Click(Sender: TObject);
begin
  mEstadistico;
end;
{$ENDREGION}

procedure TFormPrincipal01.Label1Click(Sender: TObject);
begin
  ShellExecute(0, 'Open', PChar(Global.Web),
    PChar(''), nil, SW_SHOWNORMAL);
end;
{$ENDREGION}

{$REGION 'Funciones Generales'}
procedure InicializarForma;
var
  mConfig: TConfig;
  mDL_TB_SISTEMA: TDL_TB_SISTEMA;
  mMensajeLicencia: string;
  mLicenciaValida: Boolean;
  mLicenciaGenerador: TLicenciaGenerador;
  mSerial, mUnidad, mDiscoDuroSerie, mMAC, mNombreArchivo: string;
  mArchivo: TStringList;
begin
  with Forma01 do
  begin

    try
      FormatSettings.ShortDateFormat := 'dd/MM/yyyy';
      FormatSettings.TimeSeparator := ':';
      FormatSettings.DecimalSeparator := '.';

      _Actualizando := False;
      InicialiarSistema;

      mConfig := TConfig.Create;
      mConfig.LeerIni;
      mConfig.Destroy;

      RzPageControl1.ActivePageIndex := 0;

      mMensajeLicencia := 'Licencia Completa';
      mLicenciaValida := LicenciaValida(mMensajeLicencia);
      if mLicenciaValida = False then
      begin
        FormActivarLicencia01._Version := GetAppVersion;
        FormActivarLicencia01._Serial := Global.Serial;
        FormActivarLicencia01._Licencia := Global.Licencia;
        FormActivarLicencia01.ShowModal;
        if FormActivarLicencia01.Tag = 0 then
        begin
          _Resultado := -1;
          _ErrorM := mMensajeLicencia;
          raise Exception.Create('');
        end;
        if FormActivarLicencia01.Tag = 1 then
        begin
          mLicenciaGenerador := TLicenciaGenerador.Create(
            Global.PathAplicacion, Global.Aplicacion);
          mLicenciaGenerador.ArchivoSerial :=
            Global.Aplicacion + '.lic';
          mUnidad := Copy(Global.PathAplicacion, 1, 3);
          mDiscoDuroSerie :=
            mLicenciaGenerador.ExtraerSerieDiscoDuro(mUnidad);
          mMAC :=
            mLicenciaGenerador.GetMACAdress;
          mLicenciaGenerador.DiscoDuroSerie := mDiscoDuroSerie;
          mLicenciaGenerador.MAC := mMAC;
          mLicenciaGenerador.Licencia := Global.Licencia;
          mLicenciaGenerador.FechaInicio := Date;
          mLicenciaGenerador.FechaFin := Date;
          mLicenciaGenerador.TipoLicencia := tlCompleta;
          mLicenciaGenerador.Instalado := 3;
          mSerial := mLicenciaGenerador.Generar_Serial;
          mArchivo := TStringList.Create;
          mArchivo.Add(mSerial);
          mNombreArchivo := Global.PathAplicacion + '\' + Global.Aplicacion + '.lic';
          mArchivo.SaveToFile(mNombreArchivo);
          mArchivo.Free;
          mLicenciaGenerador.Destroy;
          mMensajeLicencia := 'Licencia Completa';
        end;
      end;

      FormPresentacion01.Show;
      FormPresentacion01.Repaint;
      Sleep(3000);

      if Global.Instalado = 0 then
      begin
        Global.DB_BaseDatos :=
          Global.PathAplicacion + '\SISFAC.FDB';
        mConfig := TConfig.Create;
        mConfig.EscribirIni_BaseDatosPathFull;
        mConfig.Destroy;
      end;

      if _Actualizando = False then
      begin
        BS_DBConexion_Conectar_Main('FB');
        if _Resultado = -1 then
        begin
          Application.MessageBox(PChar('Ha ocurrido un error' + #13+#10 + _ErrorM),
            'Error', MB_ICONERROR);
        end;
        if _Resultado = 1 then
        begin
          Global.ImpuestoServicio := 0;
          Global.APLICARREDONDEO := 0;
          Global.APLICARREDONDEO5 := 0;
          Global.ImpuestoIncluido := 0;
          Global.USARSEGURIDAD := 0;
          Global.USARCONTABILIDAD := 0;

          mDL_TB_SISTEMA := TDL_TB_SISTEMA.Create;
          mDL_TB_SISTEMA.Consultar(_Resultado, _ErrorM);
          with mDL_TB_SISTEMA.Dataset do
          begin
            if IsEmpty = False then
            begin
              Global.ImpuestoServicio :=
                FieldByName('ISERVICIO').AsFloat;
              Global.APLICARREDONDEO :=
                FieldByName('APLICARREDONDEO').AsInteger;
              Global.APLICARREDONDEO5 :=
                FieldByName('APLICARREDONDEO5').AsInteger;
              Global.ImpuestoIncluido :=
                FieldByName('PRODUCTOIMPUESTOINCLUIDO').AsInteger;
              Global.USARSEGURIDAD :=
                FieldByName('USARSEGURIDAD').AsInteger;
              Global.USARCONTABILIDAD :=
                FieldByName('USARCONTABILIDAD').AsInteger;
            end;
          end;
          mDL_TB_SISTEMA.Destroy;
        end;
      end;

      if Global.Instalado = 0 then
      begin
        FormOpcionesIniciales01.ShowModal;
        if FormOpcionesIniciales01.Tag = 1 then
        begin
          Global.Instalado := 1;
          mConfig := TConfig.Create;
          mConfig.EscribirIni_Instalado;
          mConfig.Destroy;
        end;
      end;

      FormPresentacion01.Close;
      lvlVersion.Caption := 'versi�n: ' + GetAppVersionFinal;
      Label1.Caption := Global.Web;
      lblLicencia.Caption := mMensajeLicencia;

      if Global.USARSEGURIDAD = 1 then
      begin
        Application.ProcessMessages;
        FormAcceso02.ShowModal;
      end;
    except
      if _Resultado = - 1 then
      begin
        Application.MessageBox(PChar('Ha ocurrido un error' + #13+#10 + _ErrorM),
          'Error', MB_ICONERROR);
      end;
      _Resultado := -1;
      Halt(0);
    end;

  end;
end;

procedure FinalizarForma;
begin
  with Forma01 do
  begin

    try
      BS_DBConexion_Desonectar_Main;
    except

    end;

  end;
end;

procedure KeyDownForma(var Key: Word; Shift: TShiftState);
begin
  with Forma01 do
  begin

    if Key = VK_ESCAPE then
    begin
      Key := 0;
      Close;
    end;

  end;
end;

{$REGION 'Funciones Menu'}
procedure mmBaseDatos;
var
  mConfig: TConfig;
begin
  with Forma01 do
  begin

    FormBaseDatos01.ShowModal;
    if FormBaseDatos01.Tag = 1 then
    begin
      BS_DBConexion_Conectar_Main('FB');
      if _Resultado = -1 then
        Application.MessageBox(PChar('Ha ocurrido un error' + #13+#10 + _ErrorM),
          'Error', MB_ICONERROR);
    end;

  end;
end;

procedure mmEmpresas;
begin
  with Forma01 do
  begin

    FormEmpresas01._Id := 1;
    FormEmpresas01.ShowModal;

  end;
end;

procedure mmUsuarios;
begin
  with Forma01 do
  begin

    FormMantenimientoUsuarios01.ShowModal;

  end;
end;

procedure mmOpciones;
begin
  with Forma01 do
  begin

    FormOpciones01.ShowModal;

  end;
end;

procedure mmPlantillas;
begin
  with Forma01 do
  begin

    FormMantenimientoPlantillas01.ShowModal;

  end;
end;

procedure mmSalir;
begin
  with Forma01 do
  begin

    Close;

  end;
end;

procedure mmFamilias;
begin
  with Forma01 do
  begin

    FormMantenimientoFamilias01.ShowModal;

  end;
end;

procedure mmGrupos;
begin
  with Forma01 do
  begin

    FormMantenimientoGrupos01.ShowModal;

  end;
end;

procedure mmProductos;
begin
  with Forma01 do
  begin

    FormMantenimientoProductos01.ShowModal;

  end;
end;

procedure mmClientes;
begin
  with Forma01 do
  begin

    FormMantenimientoClientes01.ShowModal;

  end;
end;

procedure mmProveedores;
begin
  with Forma01 do
  begin

    FormMantenimientoProveedores01.ShowModal;

  end;
end;

procedure mmVendedores;
begin
  with Forma01 do
  begin

    FormMantenimientoVendedores01.ShowModal;

  end;
end;

procedure mmImpuestos;
begin
  with Forma01 do
  begin

    FormMantenimientoImpuestos01.ShowModal;

  end;
end;

procedure mmCambiarPrecios;
begin
  with Forma01 do
  begin

    FormCambiarPrecios01.ShowModal;

  end;
end;

procedure mmCompras;
begin
  with Forma01 do
  begin

    FormMantenimientoFacturasProveedores01.ShowModal;

  end;
end;

procedure mmRecibosProveedores;
begin
  with Forma01 do
  begin

    FormMantenimientoRecibosProveedores01.ShowModal;

  end;
end;

procedure mmFacturacion;
begin
  with Forma01 do
  begin

    FormMantenimientoFacturasClientes01.ShowModal;

  end;
end;

procedure mmProformas;
begin
  with Forma01 do
  begin

    FormMantenimientoProformas01.ShowModal;

  end;
end;

procedure mmApartados;
begin
  with Forma01 do
  begin

    FormMantenimientoApartados01.ShowModal;

  end;
end;

procedure mmRecibosClientes;
begin
  with Forma01 do
  begin

    FormMantenimientoRecibosClientes01.ShowModal;

  end;
end;

procedure mmCierresCaja;
begin
  with Forma01 do
  begin

    FormAcceso01.ShowModal;
    if FormAcceso01.Tag = 1 then
      FormMantenimientoRecibosCierresCaja01.ShowModal;

  end;
end;

procedure mmOrdenes;
begin
  with Forma01 do
  begin

    FormMantenimientoOrdenes01.ShowModal;

  end;
end;

procedure mmCuentas;
begin
  with Forma01 do
  begin

    FormMantenimientoCuentas01.ShowModal;

  end;
end;

procedure mmAsientos;
begin
  with Forma01 do
  begin

    FormMantenimientoAsientos01.ShowModal;

  end;
end;

procedure mmRecibosApartados;
begin
  with Forma01 do
  begin

    FormMantenimientoRecibosApartados01.ShowModal;

  end;
end;

procedure mmManual;
var
  mArchivo: string;
begin
  with Forma01 do
  begin

    mArchivo := Global.PathAplicacion + '\Manual de Usuario Sisfac.pdf';
    if FileExists(mArchivo) = True then
      ShellExecute(0, 'Open', PChar(mArchivo),
        PChar(''), nil, SW_SHOWNORMAL);

  end;
end;

procedure mmAcercaDe;
begin
  with Forma01 do
  begin

    FormAcercaDe01.ShowModal;

  end;
end;

procedure mmLicencia;
begin
  with Forma01 do
  begin

    FormPedirLicencia01.ShowModal;

  end;
end;

procedure mmFacturas_Nuevos;
begin
  with Forma01 do
  begin

    FormFacturaClientes01._IdCliente := 0;
    FormFacturaClientes01._Orden := 0;
    FormFacturaClientes01._Origen := 1;
    FormFacturaClientes01._IdEstado := 1;
    FormFacturaClientes01._IdFormaPago := 0;
    FormFacturaClientes01._IdDocumento := 0;
    FormFacturaClientes01._Numero := 0;
    FormFacturaClientes01.WindowState := wsMaximized;
    FormFacturaClientes01.ShowModal;

  end;
end;

procedure mmProformas_Nuevos;
begin
  with Forma01 do
  begin

    FormProformas01._IdEstado := 1;
    FormProformas01._IdFormaPago := 0;
    FormProformas01._IdDocumento := 0;
    FormProformas01._Numero := 0;
    FormProformas01.WindowState := wsMaximized;
    FormProformas01.ShowModal;

  end;
end;

procedure mmEntradas_Nuevos;
begin
  with Forma01 do
  begin

    FormFacturasProveedores01._IdEstado := 1;
    FormFacturasProveedores01._IdFormaPago := 0;
    FormFacturasProveedores01._IdDocumento := 0;
    FormFacturasProveedores01._Numero := 0;
    FormFacturasProveedores01.WindowState := wsMaximized;
    FormFacturasProveedores01.ShowModal;

  end;
end;

procedure mmApartados_Nuevos;
begin
  with Forma01 do
  begin

    FormApartados01._IdEstado := 1;
    FormApartados01._IdFormaPago := 0;
    FormApartados01._IdDocumento := 0;
    FormApartados01._Numero := 0;
    FormApartados01.WindowState := wsMaximized;
    FormApartados01.ShowModal;

  end;
end;

procedure mmRecibosClientes_Nuevos;
begin
  with Forma01 do
  begin

    FormRecibosClientes01._IdEstado := 1;
    FormRecibosClientes01._IdFormaPago := 0;
    FormRecibosClientes01._IdDocumento := 0;
    FormRecibosClientes01._Numero := 0;
    FormRecibosClientes01.WindowState := wsMaximized;
    FormRecibosClientes01.ShowModal;

  end;
end;

procedure mmCierresCaja_Nuevos;
begin
  with Forma01 do
  begin

    FormCierresCaja01._IdEstado := 1;
    FormCierresCaja01._IdFormaPago := 0;
    FormCierresCaja01._IdDocumento := 0;
    FormCierresCaja01._Numero := 0;
    FormCierresCaja01.ShowModal;

  end;
end;

procedure mmListadoFamlias;
begin
  with Forma01 do
  begin

    ReptListadoFamilias01.ShowModal;

  end;
end;

procedure mmListadoProductos;
begin
  with Forma01 do
  begin

    ReptListadoProductos01.ShowModal;

  end;
end;

procedure mmListadoClientes;
begin
  with Forma01 do
  begin

    ReptListadoClientes01.ShowModal;

  end;
end;

procedure mmListadoProveedores;
begin
  with Forma01 do
  begin

    ReptListadoProveedores01.ShowModal;

  end;
end;

procedure mmListadoVendedores;
begin
  with Forma01 do
  begin

    ReptListadoVendedores01.ShowModal;

  end;
end;

procedure mmFacturasProveedores;
begin
  with Forma01 do
  begin

    ReptFacturasProveedores01.ShowModal;

  end;
end;

procedure mmRecibosAbonosProveedores;
begin
  with Forma01 do
  begin

    ReptRecibosProveedores01.ShowModal;

  end;
end;

procedure mmFacturasClientes;
begin
  with Forma01 do
  begin

    ReptFacturasClientes01.ShowModal;

  end;
end;

procedure mmRecibosAbonosClientes;
begin
  with Forma01 do
  begin

    ReptRecibosClientes01.ShowModal;

  end;
end;

procedure mmReporteApartados;
begin
  with Forma01 do
  begin

    ReptApartados01.ShowModal;

  end;
end;

procedure mmRecibosAbonosApartados;
begin
  with Forma01 do
  begin

    ReptRecibosApartados01.ShowModal;

  end;
end;

procedure mmReporteProformas;
begin
  with Forma01 do
  begin

    ReptProformas01.ShowModal;

  end;
end;

procedure mmReporteCierresCaja;
begin
  with Forma01 do
  begin

    FormAcceso01.ShowModal;
    if FormAcceso01.Tag = 1 then
      ReptCierresCaja01.ShowModal;

  end;
end;

procedure mmReporteCompras;
begin
  with Forma01 do
  begin

    ReptCompras01.ShowModal;

  end;
end;

procedure mmReporteVentas;
begin
  with Forma01 do
  begin

    ReptVentas01.ShowModal;

  end;
end;

procedure mmEstadoCuenta;
begin
  with Forma01 do
  begin

    ReptEstadoCuenta01.ShowModal;

  end;
end;

procedure mmReporteNotasCredito;
begin
  with Forma01 do
  begin

    ReptNotasCredito01.ShowModal;

  end;
end;

procedure mmUtilidad;
begin
  with Forma01 do
  begin

    ReptUtilidad01.ShowModal;

  end;
end;

procedure mmValorInventario;
begin
  with Forma01 do
  begin

    ReptValorInventario01.ShowModal;

  end;
end;

procedure mEstadistico;
begin
  with Forma01 do
  begin

    ReptEstadistico01.ShowModal;

  end;
end;
{$ENDREGION}

function LicenciaValida(var pMensaje: string): Boolean;
var
  mLicenciaGenerador: TLicenciaGenerador;
  mResultado: Boolean;
  mUnidad: string;
  mDiscoDuroSerie: string;
  mMAC: string;
begin
  with Forma01 do
  begin

    try
      mLicenciaGenerador := TLicenciaGenerador.Create(
        Global.PathAplicacion, Global.Aplicacion);
      mLicenciaGenerador.ArchivoSerial :=
        Global.Aplicacion + '.lic';
      mUnidad := Copy(Global.PathAplicacion, 1, 3);
      mDiscoDuroSerie :=
        mLicenciaGenerador.ExtraerSerieDiscoDuro(mUnidad);
      mMAC :=
        mLicenciaGenerador.GetMACAdress;
      mResultado :=
        mLicenciaGenerador.EsSerialValido2(pMensaje,
          Global.Aplicacion, mDiscoDuroSerie, mMAC);
      Global.Licencia := mLicenciaGenerador.Licencia;
      Global.Serial := mLicenciaGenerador.Serial;
      mLicenciaGenerador.Destroy;
    except
    end;

    Result := mResultado;

  end;
end;
{$ENDREGION}

end.
