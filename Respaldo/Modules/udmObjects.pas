unit udmObjects;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.DateUtils,
  Vcl.Dialogs, Vcl.ComCtrls, FireDAC.UI.Intf, FireDAC.Comp.ScriptCommands,
  FireDAC.Stan.Util, FireDAC.Comp.Script, System.Variants;

type
  TdmObjects = class(TDataModule)
    FDQuery1: TFDQuery;
    FDQuery2: TFDQuery;
    FDQuery3: TFDQuery;
    FDQuery4: TFDQuery;
    FDQuery5: TFDQuery;
    FDQuery6: TFDQuery;
    FDQuery7: TFDQuery;
    FDQuery8: TFDQuery;
    FDStoredProc1: TFDStoredProc;
    FDStoredProc2: TFDStoredProc;
    FDStoredProc3: TFDStoredProc;
    FDStoredProc4: TFDStoredProc;
    FDStoredProc5: TFDStoredProc;
    FDStoredProc6: TFDStoredProc;
    FDScript1: TFDScript;
    FDScript2: TFDScript;
    FDScript3: TFDScript;
    FDScript4: TFDScript;
    FDScript5: TFDScript;
    FDScript6: TFDScript;
  private
    { Private declarations }
  public
    {$REGION 'General'}
    procedure DB_Asignar_Id(qry: TFDQuery; pTabla: string; ds: TFDMemTable;
      var pResultado: Integer; var pErrorM: string);
    function DB_Obtener_Ultimo(qry: TFDQuery; pTabla: string;
      var pResultado: Integer; var pErrorM: string): Integer; overload;
    function DB_Obtener_Ultimo(qry: TFDQuery; pTabla: string;
      pCampo: string; pWhere: string; var pResultado: Integer;
      var pErrorM: string): Integer; overload;
    procedure DB_DBToStruct(qry: TFDQuery; ds1: TFDMemTable;
      var pResultado: Integer; var pErrorM: string); overload;
    procedure DB_DBToStruct(qry: TFDQuery; ds1: TFDMemTable;
      var pCamposNombre: TStringList;
      var pResultado: Integer; var pErrorM: string); overload;
    procedure DB_StructToDB(qry: TFDQuery; ds1: TFDMemTable;
      pCampos: TStringList; var pResultado: Integer; var pErrorM: string);
    function DB_Obtener_ListaCampos(ds1: TFDMemTable): TStringList;
    function DB_Crear_SQL_Insert(pTabla: string; pCampos: string): string;
    function DB_Crear_SQL_Update(pTabla: string; pCampos: string): string;
    function DB_Obtener_SQL_ParametrosLista(
      pCampos: TStringList): string;
    procedure DB_Limpiar_Tag(ds1: TFDMemTable);
    procedure DB_Insert_Generico(qry1: TFDQuery; ds1: TFDMemTable;
      var pResultado: Integer; var pErrorM: string);
    procedure DB_Update_Generico(qry1: TFDQuery; ds1: TFDMemTable;
      pWhere: string; var pResultado: Integer; var pErrorM: string);
    procedure DB_Delete_Generico(qry1: TFDQuery; pTabla: string;
      pWhere: string; var pResultado: Integer; var pErrorM: string);
    procedure DB_Copiar_Estructura(dsOrigen: TFDMemTable;
      dsDestino: TFDMemTable; var pResultado: Integer; var pErrorM: string);
    procedure DB_Copiar_Datos(dsOrigen: TFDMemTable;
      dsDestino: TFDMemTable; var pResultado: Integer; var pErrorM: string);
    procedure DB_Consultar_Generico(qry1: TFDQuery; ds1: TFDMemTable;
      pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
      pSelect: string; var pCamposNombre: TStringList;
      var pResultado: Integer; var pErrorM: string);
    function GetNombreTab(pNombreFull: string; pNombreSimple: string): string;
    {$ENDREGION}

    {$REGION 'Skeleton'}
    procedure DB_Skeleton_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
      pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
      pTabla: string; pClaseNombre: string; var pResultado: Integer;
      var pErrorM: string);
    procedure DB_Skeleton_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
      pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
    procedure DB_Skeleton_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
      pWhere: string; pClaseNombre: string; var pResultado: Integer;
      var pErrorM: string);
    procedure DB_Skeleton_Eliminar(qry1: TFDQuery; pWhere: string;
      pTabla: string; pClaseNombre: string; var pResultado: Integer;
      var pErrorM: string);
    function DB_Skeleton_Existe_Id(qry1: TFDQuery; pWhere: string;
      pTabla: string; pClaseNombre: string; var pResultado: Integer;
      var pErrorM: string): Boolean;
    function DB_Skeleton_Obtener_Id(qry1: TFDQuery; pWhere: string;
      pTabla: string; pClaseNombre: string; var pResultado: Integer;
      var pErrorM: string): Integer;
    function DB_Skeleton_Existe_Campo(qry1: TFDQuery; pWhere: string;
      pTabla: string; pCampo: string; pClaseNombre: string;
      var pResultado: Integer; var pErrorM: string): Boolean;
    function DB_Skeleton_Obtener_Valor(qry1: TFDQuery; pWhere: string;
      pTabla: string; pCampo: string; pClaseNombre: string;
      var pResultado: Integer; var pErrorM: string): Variant;
    procedure DB_Skeleton_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
      pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
      var pResultado: Integer; var pErrorM: string);
    {$ENDREGION}

    //Declaracion Metodos
  {$REGION 'TB_APARTADOSDETALLE'}
  procedure DB_TB_APARTADOSDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_APARTADOSDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_APARTADOSDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_APARTADOSDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_APARTADOSDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_APARTADOSDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_APARTADOSDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_APARTADOSDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_APARTADOSDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_APARTADOSENCABEZADO'}
  procedure DB_TB_APARTADOSENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_APARTADOSENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_APARTADOSENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_APARTADOSENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_APARTADOSENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_APARTADOSENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_APARTADOSENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_APARTADOSENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_APARTADOSENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_APARTADOSENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_CIERRESCAJAENCABEZADO'}
  procedure DB_TB_CIERRESCAJAENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_CIERRESCAJAENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_CIERRESCAJAENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_CIERRESCAJAENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_CIERRESCAJAENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_CIERRESCAJAENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_CIERRESCAJAENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_CIERRESCAJAENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_CIERRESCAJAENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_CLIENTES'}
  procedure DB_TB_CLIENTES_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_CLIENTES_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_CLIENTES_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_CLIENTES_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_CLIENTES_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_CLIENTES_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_CLIENTES_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_CLIENTES_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_CLIENTES_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_CLIENTES_Sumar_Saldo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pValor: Double; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_CLIENTES_Restar_Saldo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pValor: Double; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_CONSECUTIVOS'}
  procedure DB_TB_CONSECUTIVOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_CONSECUTIVOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_CONSECUTIVOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_CONSECUTIVOS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_CONSECUTIVOS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_CONSECUTIVOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_CONSECUTIVOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_CONSECUTIVOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_CONSECUTIVOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_EMPRESAS'}
  procedure DB_TB_EMPRESAS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_EMPRESAS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_EMPRESAS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_EMPRESAS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_EMPRESAS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_EMPRESAS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_EMPRESAS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_EMPRESAS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_EMPRESAS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_ENTRADASDETALLE'}
  procedure DB_TB_ENTRADASDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_ENTRADASDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ENTRADASDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_ENTRADASDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_ENTRADASDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_ENTRADASDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_ENTRADASDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_ENTRADASDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_ENTRADASDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ENTRADASDETALLE_Sumar_Existencias(qry1: TFDQuery; pWhere: string;
    pTabla: string; d1: TFDMemTable; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ENTRADASDETALLE_Restar_Existencias(qry1: TFDQuery; pWhere: string;
    pTabla: string; d1: TFDMemTable; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_ENTRADASENCABEZADO'}
  procedure DB_TB_ENTRADASENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_ENTRADASENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ENTRADASENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_ENTRADASENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_ENTRADASENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_ENTRADASENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_ENTRADASENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_ENTRADASENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_ENTRADASENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ENTRADASENCABEZADO_Sumar_Totales(qry1: TFDQuery; pWhere: string;
    pClaseNombre: string; var pSubTotal: Double;
    var pDescuento: Double; var pTotal: Double;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ENTRADASENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_ESTADOS'}
  procedure DB_TB_ESTADOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_ESTADOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_ESTADOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_ESTADOS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_ESTADOS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_ESTADOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_ESTADOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_ESTADOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_ESTADOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_FACTURASCLIENTESDETALLE'}
  procedure DB_TB_FACTURASCLIENTESDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_FACTURASCLIENTESDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_FACTURASCLIENTESDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_FACTURASCLIENTESDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_FACTURASCLIENTESDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_FACTURASCLIENTESDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESDETALLE_Sumar_Existencias(qry1: TFDQuery; pWhere: string;
    pTabla: string; d1: TFDMemTable; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESDETALLE_Restar_Existencias(qry1: TFDQuery; pWhere: string;
    pTabla: string; d1: TFDMemTable; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_Detalles(qry1: TFDQuery; pWhere: string;
    pWhere2: string;
    pTabla: string; pClaseNombre: string; ds1: TFDMemTable;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_FACTURASCLIENTESENCABEZADO'}
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_FACTURASCLIENTESENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_FACTURASCLIENTESENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Actualizar_Saldo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pValor: Double; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_TotalesPago(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pEfectivo: Double; var pTarjeta: Double;
    var pCheque: Double; var pTransferencia: Double;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Sumar_Totales(qry1: TFDQuery; pWhere: string;
    pClaseNombre: string; var pSubTotal: Double;
    var pDescuento: Double; var pServicio: Double;
    var pIVA: Double;var pTotal: Double;
    var pResultado: Integer; var pErrorM: string);
  function DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_SaldoFecha(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Double;
  procedure DB_TB_FACTURASCLIENTESENCABEZADO_Estadistico(qry1: TFDQuery; pWhere: string;
    ds1: TFDMemTable; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_FAMILIAS'}
  procedure DB_TB_FAMILIAS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FAMILIAS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FAMILIAS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FAMILIAS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_FAMILIAS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_FAMILIAS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_FAMILIAS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_FAMILIAS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_FAMILIAS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_FORMASPAGO'}
  procedure DB_TB_FORMASPAGO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FORMASPAGO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_FORMASPAGO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_FORMASPAGO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_FORMASPAGO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_FORMASPAGO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_FORMASPAGO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_FORMASPAGO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_FORMASPAGO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_IMPUESTOS'}
  procedure DB_TB_IMPUESTOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_IMPUESTOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_IMPUESTOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_IMPUESTOS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_IMPUESTOS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_IMPUESTOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_IMPUESTOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_IMPUESTOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_IMPUESTOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  function DB_TB_IMPUESTOS_Obtener_Porcentaje(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Double;
  function DB_TB_IMPUESTOS_Obtener_PorcentajeValor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Double;
  {$ENDREGION}

  {$REGION 'TB_NOTASCREDITOENCABEZADO'}
  procedure DB_TB_NOTASCREDITOENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_NOTASCREDITOENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_NOTASCREDITOENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_NOTASCREDITOENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_NOTASCREDITOENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_NOTASCREDITOENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_NOTASCREDITOENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_NOTASCREDITOENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_NOTASCREDITOENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_NOTASDEBITOENCABEZADO'}
  procedure DB_TB_NOTASDEBITOENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_NOTASDEBITOENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_NOTASDEBITOENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_NOTASDEBITOENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_NOTASDEBITOENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_NOTASDEBITOENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_NOTASDEBITOENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_NOTASDEBITOENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_NOTASDEBITOENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_PLANTILLAS'}
  procedure DB_TB_PLANTILLAS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PLANTILLAS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PLANTILLAS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PLANTILLAS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_PLANTILLAS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_PLANTILLAS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_PLANTILLAS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_PLANTILLAS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_PLANTILLAS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  function DB_TB_PLANTILLAS_Obtener_Plantilla(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): string;
  {$ENDREGION}

  {$REGION 'TB_PRODUCTOS'}
  procedure DB_TB_PRODUCTOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PRODUCTOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PRODUCTOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PRODUCTOS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_PRODUCTOS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_PRODUCTOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_PRODUCTOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_PRODUCTOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_PRODUCTOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  function DB_TB_PRODUCTOS_Obtener_ImpuestoPorcentaje(qry1: TFDQuery;
    pWhere: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Double;
  function DB_TB_PRODUCTOS_Obtener_Existencia(qry1: TFDQuery;
    pWhere: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Double;
  procedure DB_TB_PRODUCTOS_Actualizar_Existencia(qry1: TFDQuery;
    pWhere: string; pExistencia: Double; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  function DB_TB_PRODUCTOS_Obtener_PrecioMayorista(qry1: TFDQuery;
    pWhere: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Double;
  function DB_TB_PRODUCTOS_Obtener_Nombre(qry1: TFDQuery;
    pWhere: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): string;
  {$ENDREGION}

  {$REGION 'TB_PROFORMASDETALLE'}
  procedure DB_TB_PROFORMASDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PROFORMASDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PROFORMASDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PROFORMASDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_PROFORMASDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_PROFORMASDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_PROFORMASDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_PROFORMASDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_PROFORMASDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_PROFORMASENCABEZADO'}
  procedure DB_TB_PROFORMASENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PROFORMASENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PROFORMASENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PROFORMASENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_PROFORMASENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_PROFORMASENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_PROFORMASENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_PROFORMASENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_PROFORMASENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PROFORMASENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_PROVEEDORES'}
  procedure DB_TB_PROVEEDORES_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PROVEEDORES_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PROVEEDORES_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_PROVEEDORES_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_PROVEEDORES_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_PROVEEDORES_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_PROVEEDORES_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_PROVEEDORES_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_PROVEEDORES_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PROVEEDORES_Sumar_Saldo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pValor: Double; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_PROVEEDORES_Restar_Saldo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pValor: Double; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_RECIBOSAPARTADOSDETALLE'}
  procedure DB_TB_RECIBOSAPARTADOSDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_RECIBOSAPARTADOSDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_RECIBOSAPARTADOSDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_RECIBOSAPARTADOSDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_RECIBOSAPARTADOSDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_RECIBOSAPARTADOSDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_RECIBOSAPARTADOSENCABEZADO'}
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_RECIBOSAPARTADOSENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_RECIBOSAPARTADOSENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_RECIBOSAPARTADOSENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_RECIBOSAPARTADOSENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Obtener_TotalesPago(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pEfectivo: Double; var pTarjeta: Double;
    var pCheque: Double; var pTransferencia: Double;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSAPARTADOSENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_RECIBOSCLIENTESDETALLE'}
  procedure DB_TB_RECIBOSCLIENTESDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_RECIBOSCLIENTESDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_RECIBOSCLIENTESDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_RECIBOSCLIENTESDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_RECIBOSCLIENTESDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_RECIBOSCLIENTESDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_RECIBOSCLIENTESENCABEZADO'}
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_RECIBOSCLIENTESENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_RECIBOSCLIENTESENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_RECIBOSCLIENTESENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_RECIBOSCLIENTESENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSCLIENTESENCABEZADO_Obtener_TotalesPago(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pEfectivo: Double; var pTarjeta: Double;
    var pCheque: Double; var pTransferencia: Double;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_RECIBOSPROVEEDORESDETALLE'}
  procedure DB_TB_RECIBOSPROVEEDORESDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_RECIBOSPROVEEDORESDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_RECIBOSPROVEEDORESDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_RECIBOSPROVEEDORESDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_RECIBOSPROVEEDORESDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_RECIBOSPROVEEDORESDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_RECIBOSPROVEEDORESENCABEZADO'}
  procedure DB_TB_RECIBOSPROVEEDORESENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_RECIBOSPROVEEDORESENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_RECIBOSPROVEEDORESENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_RECIBOSPROVEEDORESENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_RECIBOSPROVEEDORESENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_RECIBOSPROVEEDORESENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_RECIBOSPROVEEDORESENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
    pTabla: string; pvalor: Integer; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_SISTEMA'}
  procedure DB_TB_SISTEMA_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_SISTEMA_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_SISTEMA_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_SISTEMA_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_SISTEMA_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_SISTEMA_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_SISTEMA_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_SISTEMA_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_SISTEMA_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_TIPOS'}
  procedure DB_TB_TIPOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_TIPOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_TIPOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_TIPOS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_TIPOS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_TIPOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_TIPOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_TIPOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_TIPOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_UNIDADESMEDIDA'}
  procedure DB_TB_UNIDADESMEDIDA_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_UNIDADESMEDIDA_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_UNIDADESMEDIDA_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_UNIDADESMEDIDA_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_UNIDADESMEDIDA_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_UNIDADESMEDIDA_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_UNIDADESMEDIDA_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_UNIDADESMEDIDA_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_UNIDADESMEDIDA_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_USUARIOS'}
  procedure DB_TB_USUARIOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_USUARIOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_USUARIOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_USUARIOS_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_USUARIOS_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_USUARIOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_USUARIOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_USUARIOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_USUARIOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_VENDEDORES'}
  procedure DB_TB_VENDEDORES_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_VENDEDORES_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_VENDEDORES_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_VENDEDORES_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_VENDEDORES_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_VENDEDORES_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_VENDEDORES_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_VENDEDORES_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_VENDEDORES_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'tb_grupos_encabezado'}
  procedure DB_TB_GruposEncabezado_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_GruposEncabezado_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_GruposEncabezado_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_GruposEncabezado_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_GruposEncabezado_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_GruposEncabezado_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_GruposEncabezado_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_GruposEncabezado_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_GruposEncabezado_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'tb_grupos_detalle'}
  procedure DB_TB_GruposDetalle_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_GruposDetalle_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_GruposDetalle_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_GruposDetalle_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_GruposDetalle_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_GruposDetalle_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_GruposDetalle_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_GruposDetalle_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_GruposDetalle_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'Ordenes Encabezado'}
  procedure DB_TB_OrdenesEncabezado_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_OrdenesEncabezado_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_OrdenesEncabezado_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_OrdenesEncabezado_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_OrdenesEncabezado_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_OrdenesEncabezado_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_OrdenesEncabezado_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_OrdenesEncabezado_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_OrdenesEncabezado_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'Ordenes Detalle'}
  procedure DB_TB_OrdenesDetalle_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_OrdenesDetalle_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_OrdenesDetalle_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_OrdenesDetalle_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_OrdenesDetalle_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_OrdenesDetalle_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_OrdenesDetalle_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_OrdenesDetalle_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_OrdenesDetalle_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_Licencias'}
  procedure DB_TB_Licencias_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_Licencias_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_Licencias_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_Licencias_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_Licencias_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_Licencias_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_Licencias_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_Licencias_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_Licencias_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_Cuentas'}
  procedure DB_TB_Cuentas_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_Cuentas_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_Cuentas_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_Cuentas_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_Cuentas_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_Cuentas_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_Cuentas_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_Cuentas_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_Cuentas_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_AsientosEncabezado'}
  procedure DB_TB_AsientosEncabezado_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_AsientosEncabezado_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_AsientosEncabezado_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_AsientosEncabezado_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_AsientosEncabezado_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_AsientosEncabezado_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_AsientosEncabezado_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_AsientosEncabezado_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_AsientosEncabezado_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_AsientosDetalle'}
  procedure DB_TB_AsientosDetalle_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_AsientosDetalle_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_AsientosDetalle_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_AsientosDetalle_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_AsientosDetalle_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_AsientosDetalle_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_AsientosDetalle_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_AsientosDetalle_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_AsientosDetalle_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_Niveles Usuarios'}
  procedure DB_TB_NivelesUsuarios_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_NivelesUsuarios_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_NivelesUsuarios_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_NivelesUsuarios_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_NivelesUsuarios_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_NivelesUsuarios_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_NivelesUsuarios_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_NivelesUsuarios_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_NivelesUsuarios_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  {$REGION 'TB_Movimientos Inventario'}
  procedure DB_TB_MovimientoInventario_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_MovimientoInventario_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
    pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
  procedure DB_TB_MovimientoInventario_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
    pWhere: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  procedure DB_TB_MovimientoInventario_Eliminar(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string);
  function DB_TB_MovimientoInventario_Existe_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Boolean;
  function DB_TB_MovimientoInventario_Obtener_Id(qry1: TFDQuery; pWhere: string;
    pTabla: string; pClaseNombre: string; var pResultado: Integer;
    var pErrorM: string): Integer;
  function DB_TB_MovimientoInventario_Existe_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Boolean;
  function DB_TB_MovimientoInventario_Obtener_Valor(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string): Variant;
  procedure DB_TB_MovimientoInventario_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
    pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
    var pResultado: Integer; var pErrorM: string);
  {$ENDREGION}

  end;

var
  dmObjects: TdmObjects;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udmConnection;

const
  SaveLog = 1;

{$R *.dfm}

{$REGION 'Funciones General'}
procedure TdmObjects.DB_Asignar_Id(qry: TFDQuery; pTabla: string;
  ds: TFDMemTable; var pResultado: Integer; var pErrorM: string);
var
  mId: Integer;
begin
  try
    pResultado := 1;

    mId := dmObjects.DB_Obtener_Ultimo(qry, pTabla, pResultado, pErrorM);
    Inc(mId);
    with ds do
    begin
      First;
      while not Eof do
      begin
        Edit;
        FieldByName('Id').AsInteger := mId;
        Post;
        Inc(mId);
        Next;
      end;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

function TdmObjects.DB_Obtener_Ultimo(qry: TFDQuery; pTabla: string;
  var pResultado: Integer; var pErrorM: string): Integer;
var
  mUltimo: Integer;
begin
  try
    pResultado := 1;

    mUltimo := 0;
    with qry do
    begin
      SQL.Clear;
      SQL.Add('Select Max(Id)');
      SQL.Add('From ' + pTabla);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mUltimo := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;

  Result := mUltimo;
end;

function TdmObjects.DB_Obtener_Ultimo(qry: TFDQuery; pTabla: string;
  pCampo: string; pWhere: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mUltimo: Integer;
begin
  try
    pResultado := 1;

    mUltimo := 0;
    with qry do
    begin
      SQL.Clear;
      SQL.Add('Select Max(' + pCampo + ')');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mUltimo := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;

  Result := mUltimo;
end;

procedure TdmObjects.DB_DBToStruct(qry: TFDQuery; ds1: TFDMemTable;
  var pResultado: Integer; var pErrorM: string);
var
  mCantFields: Integer;
  mCols: Integer;
  CampoNombre: string;
begin
  try
    pResultado := 1;

    mCantFields := 0;
    with qry do
    begin
      Open();
      mCantFields := FieldCount;
      while not Eof do
      begin
        ds1.Append;
        for mCols := 0 to mCantFields - 1 do
        begin
          CampoNombre := FieldDefs[mCols].Name;
          ds1.FieldByName(CampoNombre).Value :=
            FieldByName(CampoNombre).Value;
        end;
        ds1.Post;

        Next;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
       pErrorM := E.Message;
       pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_DBToStruct(qry: TFDQuery; ds1: TFDMemTable;
  var pCamposNombre: TStringList; var pResultado: Integer; var pErrorM: string);
var
  mCantFields: Integer;
  mCols: Integer;
  CampoNombre: string;
begin
  try
    pResultado := 1;

    pCamposNombre.Clear;

    ds1.EmptyDataSet;
    ds1.Close;
    ds1.FieldDefs.Clear;

    mCantFields := 0;
    with qry do
    begin
      Open();
      mCantFields := FieldCount;
      for mCols := 0 to mCantFields - 1 do
      begin
        ds1.FieldDefs.Add(FieldDefs[mCols].Name,
          FieldDefs[mCols].DataType, FieldDefs[mCols].Size);
        pCamposNombre.Add(FieldDefs[mCols].Name);
      end;
      ds1.CreateDataSet;
      ds1.Open;

      while not Eof do
      begin
        ds1.Append;
        for mCols := 0 to mCantFields - 1 do
        begin
          CampoNombre := FieldDefs[mCols].Name;
          ds1.FieldByName(CampoNombre).Value :=
            FieldByName(CampoNombre).Value;
        end;
        ds1.Post;

        Next;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
       pErrorM := E.Message;
       pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_StructToDB(qry: TFDQuery; ds1: TFDMemTable;
  pCampos: TStringList; var pResultado: Integer; var pErrorM: string);
var
  mCantFields: Integer;
  mCols, mFila: Integer;
  CampoNombre: string;
begin
  try
    pResultado := 1;

    for mCols := 0 to pCampos.Count - 1 do
    begin
      with qry do
      begin
        Params.Items[mCols].Value :=
          ds1.Fields[mCols].Value;
      end;
    end;
    qry.ExecSQL;

    pResultado := 1;
  except
    on E: Exception do
    begin
       pErrorM := E.Message;
       pResultado := -1;
    end;
  end;
end;

function TdmObjects.DB_Obtener_ListaCampos(ds1: TFDMemTable): TStringList;
var
  mCampos: TStringList;
  mCols: Integer;
begin
  mCampos := TStringList.Create;

  mCampos.Clear;
  mCampos.Delimiter :=',';
  with ds1 do
  begin
    for mCols := 0 to FieldCount - 1 do
    begin
      if Fields[mCols].Tag = 1  then
        mCampos.Add(FieldDefs[mCols].Name);
    end;
  end;

  Result := mCampos;
end;

function TdmObjects.DB_Crear_SQL_Insert(pTabla: string; pCampos: string): string;
var
  mResultado: string;
begin
  mResultado := '';

  mResultado := 'Insert Into ' + pTabla;
  mResultado := mResultado + '(';
  mResultado := mResultado + pCampos;
  mResultado := mResultado + ')';

  Result := mResultado;
end;

function TdmObjects.DB_Crear_SQL_Update(pTabla: string;
  pCampos: string): string;
var
  mResultado: string;
begin
  mResultado := '';

  mResultado := 'Update  ' + pTabla + ' Set ';
  mResultado := mResultado + pCampos;

  Result := mResultado;
end;

function TdmObjects.DB_Obtener_SQL_ParametrosLista(
  pCampos: TStringList): string;
var
  mCountParams: Integer;
  Aux01: Integer;
  mParams: TStringList;
  mSQL: string;
begin
  mSQL := '';

  mCountParams := pCampos.Count - 1;
  mParams := TStringList.Create;
  mParams.Clear;
  mParams.Delimiter := ',';
  for Aux01 := 0 to mCountParams do
    mParams.Add(':' + pCampos.Strings[Aux01]);
  mSQL := 'Values(' + mParams.DelimitedText + ')';
  mParams.Free;

  Result := mSQL;
end;

procedure TdmObjects.DB_Limpiar_Tag(ds1: TFDMemTable);
var
  mCols: Integer;
begin
  with ds1 do
  begin
    for mCols := 0 to FieldCount - 1 do
      Fields[mCols].Tag := 0;
  end;
end;

procedure TdmObjects.DB_Insert_Generico(qry1: TFDQuery; ds1: TFDMemTable;
  var pResultado: Integer; var pErrorM: string);
var
  mCampos: TStringList;
  mSQL, mSQL1, mSQL2: string;
  mCols: Integer;
begin
  try
    pResultado := 1;

    mSQL1 := '';
    mSQL2 := '';

    mCampos := TStringList.Create;

    mCampos :=
      DB_Obtener_ListaCampos(ds1);

    mSQL1 :=
      DB_Crear_SQL_Insert(ds1.FileName, mCampos.DelimitedText);

    mSQL2 :=
      DB_Obtener_SQL_ParametrosLista(mCampos);

    qry1.SQL.Clear;
    mSQL := mSQL1 + ' ' + mSQL2;

    with ds1 do
    begin
      First;
      while not Eof do
      begin
        qry1.SQL.Clear;
        qry1.SQL.Add(mSQL);

        for mCols := 0 to mCampos.Count - 1 do
          qry1.ParamByName(mCampos.Strings[mCols]).Value :=
          FieldByName(mCampos.Strings[mCols]).Value;

        qry1.ExecSQL;

        Next;
      end;
    end;
    mCampos.Free;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_Update_Generico(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; var pResultado: Integer; var pErrorM: string);
var
  mCampos, mCamposParams: TStringList;
  mSQL, mSQL1, mSQL2: string;
  mCols: Integer;
  Aux01: Integer;
begin
  try
    pResultado := 1;

    mSQL1 := '';
    mSQL2 := '';

    mCampos := TStringList.Create;
    mCamposParams := TStringList.Create;

    mCampos :=
      DB_Obtener_ListaCampos(ds1);

    for Aux01 := 0 to mCampos.Count - 1 do
    begin
      if Aux01 < (mCampos.Count - 1) then
        mCamposParams.Add(mCampos.Strings[Aux01] + ' = :' +
          mCampos.Strings[Aux01] + ',')
      else
        mCamposParams.Add(mCampos.Strings[Aux01] + ' = :' +
          mCampos.Strings[Aux01]);
    end;
    mCampos.Delimiter := ',';
    mCamposParams.Delimiter := ',';

    mSQL1 :=
      DB_Crear_SQL_Update(ds1.FileName, mCamposParams.Text);

    qry1.SQL.Clear;
    mSQL := mSQL1 + ' ' + pWhere;
    qry1.SQL.Add(mSQL);

    with ds1 do
    begin
      First;
      while not Eof do
      begin
        qry1.SQL.Clear;
        qry1.SQL.Add(mSQL);

        for mCols := 0 to mCampos.Count - 1 do
          qry1.ParamByName(mCampos.Strings[mCols]).Value :=
          FieldByName(mCampos.Strings[mCols]).Value;

        qry1.ExecSQL;

        Next;
      end;
    end;
    mCampos.Free;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_Delete_Generico(qry1: TFDQuery; pTabla: string;
  pWhere: string; var pResultado: Integer; var pErrorM: string);
var
  mSQL: string;
begin
  try
    pResultado := 1;

    mSQL:= 'Delete From ' + pTabla + ' ' + pWhere;

    qry1.SQL.Clear;
    qry1.SQL.Add(mSQL);
    qry1.ExecSQL;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_Copiar_Estructura(dsOrigen: TFDMemTable;
  dsDestino: TFDMemTable; var pResultado: Integer; var pErrorM: string);
var
  Aux01: Integer;
begin
  try
    pResultado := 1;

    dsDestino.FileName := dsOrigen.FileName;
    dsDestino.FieldDefs.Clear;
    with dsOrigen do
    begin
      for Aux01 := 0 to FieldCount - 1 do
      begin
        dsDestino.FieldDefs.Add(FieldDefs[Aux01].Name,
          FieldDefs[Aux01].DataType, FieldDefs[Aux01].Size);
      end;
    end;
    dsDestino.CreateDataSet;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_Copiar_Datos(dsOrigen: TFDMemTable;
  dsDestino: TFDMemTable; var pResultado: Integer; var pErrorM: string);
var
  Aux01: Integer;
begin
  try
    pResultado := 1;

    dsDestino.FileName := dsOrigen.FileName;
    dsDestino.EmptyDataSet;
    with dsOrigen do
    begin
      First;
      while not Eof do
      begin
        dsDestino.Append;
        for Aux01 := 0 to FieldCount - 1 do
        begin
          dsDestino.Fields[Aux01].Value :=
            Fields[Aux01].Value;
        end;
        dsDestino.Post;
        Next;
      end;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

procedure TdmObjects.DB_Consultar_Generico(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pSelect: string; var pCamposNombre: TStringList;
  var pResultado: Integer; var pErrorM: string);
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add(pSelect);
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);

      DB_DBToStruct(qry1, ds1, pCamposNombre, pResultado, pErrorM);

      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
    end;
  end;
end;

function TdmObjects.GetNombreTab(pNombreFull: string; pNombreSimple: string): string;
var
  mResultado: string;
  mNombreFull: string;
  mLista: TStringList;
  mCount: Integer;
begin
  mResultado := '';
  mNombreFull := pNombreFull;

  ExtractStrings([':'], [':'], PChar(mNombreFull), mLista);
  mCount := mLista.Count;
  mCount := mCount * 2;
  mResultado := StringOfChar(' ', mCount) + pNombreSimple;

  Result := mResultado;
end;
{$ENDREGION}

{$REGION 'Skeleton'}
procedure TdmObjects.DB_Skeleton_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.*');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;

      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');

      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_Skeleton_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
      mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Insertar.sql');
      mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_Skeleton_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
      mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Modificar.sql');
      mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_Skeleton_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
      mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Eliminar.sql');
      mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_Skeleton_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From  ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_Skeleton_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From  ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_Skeleton_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ' + pCampo);
      SQL.Add('From  ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_Skeleton_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ' + pCampo);
      SQL.Add('From  ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_Skeleton_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + ' = ' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre + '_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

//Implementacion Metodos
{$REGION 'TB_APARTADOSDETALLE'}
procedure TdmObjects.DB_TB_APARTADOSDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CANTIDAD,');
      SQL.Add('A.CODIGOUNIDADMEDIDA,');
      SQL.Add('A.NOMBREUNIDADMEDIDA,');
      SQL.Add('A.PRECIOUNITARIO,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.PRECIOUNITARIOFINAL,');
      SQL.Add('A.PRECIOFINAL,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOIMPUESTO,');
      SQL.Add('A.NOMBREIMPUESTO,');
      SQL.Add('A.PRECIOMAYORISTAAPLICADO,');
      SQL.Add('A.IdProducto');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_APARTADOSDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_APARTADOSDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_APARTADOSDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_APARTADOSDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_APARTADOSDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_APARTADOSENCABEZADO'}
procedure TdmObjects.DB_TB_APARTADOSENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.CODIGOMESA,');
      SQL.Add('A.NUMEROMESA,');
      SQL.Add('A.CODIGOSALONERO,');
      SQL.Add('A.USUARIOSALONERO,');
      SQL.Add('A.NOMBRESALONERO,');
      SQL.Add('A.CODIGOCLIENTE,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.SUBTOTAL,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.ISP,');
      SQL.Add('A.ISM,');
      SQL.Add('A.TRANSPORTE,');
      SQL.Add('A.TOTAL,');
      SQL.Add('A.TOTALGRAVADO,');
      SQL.Add('A.TOTALEXCENTO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.SALDO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.NUMEROFACTURA,');
      SQL.Add('A.NUMEROSFACTURA,');
      SQL.Add('A.PAGOCON,');
      SQL.Add('A.CAMBIO,');
      SQL.Add('A.IdCliente,');
      SQL.Add('A.IdVendedor,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_APARTADOSENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_APARTADOSENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_APARTADOSENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_APARTADOSENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_APARTADOSENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_APARTADOSENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_CIERRESCAJAENCABEZADO'}
procedure TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.IDMAQUINA,');
      SQL.Add('A.FECHADE,');
      SQL.Add('A.FECHAHASTA,');
      SQL.Add('A.HORADE,');
      SQL.Add('A.HORAHASTA,');
      SQL.Add('A.MONTOINICIAL,');
      SQL.Add('A.MONTOFINAL,');
      SQL.Add('A.TOTALCANTIDADENTRADAS,');
      SQL.Add('A.TOTALCANTIDADSALIDAS,');
      SQL.Add('A.TOTALMONTOENTRADAS,');
      SQL.Add('A.TOTALMONTOSALIDAS,');
      SQL.Add('A.TOTALCANTIDADEFECTIVO,');
      SQL.Add('A.TOTALCANTIDADTARJETA,');
      SQL.Add('A.TOTALCANTIDADTRANSFERENCIA,');
      SQL.Add('A.TOTALCANTIDADCHEQUE,');
      SQL.Add('A.TOTALCANTIDADCREDITO,');
      SQL.Add('A.TOTALMONTOEFECTIVO,');
      SQL.Add('A.TOTALMONTOTARJETA,');
      SQL.Add('A.TOTALMONTOTRANSFERENCIA,');
      SQL.Add('A.TOTALMONTOCHEQUE,');
      SQL.Add('A.TOTALMONTOCREDITO,');
      SQL.Add('A.COMENTARIOS');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_CIERRESCAJAENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_CLIENTES'}
procedure TdmObjects.DB_TB_CLIENTES_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.FECHAALTA,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.APELLIDO1,');
      SQL.Add('A.APELLIDO2,');
      SQL.Add('A.NOMBRECOMPLETO,');
      SQL.Add('A.CODIGOPOSTAL,');
      SQL.Add('A.DIRECCION,');
      SQL.Add('A.EMAIL,');
      SQL.Add('A.TELEFONO1,');
      SQL.Add('A.TELEFONO2,');
      SQL.Add('A.WEB,');
      SQL.Add('A.NOMBREIMPRIMIR,');
      SQL.Add('A.CEDULA,');
      SQL.Add('A.IMAGEN1,');
      SQL.Add('A.NOMBREARCHIVO,');
      SQL.Add('A.FECHANACIMIENTO,');
      SQL.Add('A.SALDO,');
      SQL.Add('A.LIMITECREDITO,');
      SQL.Add('A.APLICARLIMITECREDITO,');
      SQL.Add('A.TIPO,');
      SQL.Add('A.PRECIOVENTAPORCENTAJE,');
      SQL.Add('A.CONTACTONOMBRE,');
      SQL.Add('A.TIPOPAGO,');
      SQL.Add('A.PLAZO,');
      SQL.Add('A.APLICARIMPUESTO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CLIENTES_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CLIENTES_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CLIENTES_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_CLIENTES_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CLIENTES_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CLIENTES_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CLIENTES_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_CLIENTES_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CLIENTES_Sumar_Saldo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pValor: Double; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mSaldo: Double;
begin
  try
    pResultado := 1;

    mSaldo := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select SALDO');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mSaldo := Fields[0].AsFloat;
      end;
      Close;
    end;

    mSaldo := mSaldo + pValor;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('SALDO =' + FloatToStr(mSaldo));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CLIENTES_Restar_Saldo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pValor: Double; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mSaldo: Double;
begin
  try
    pResultado := 1;

    mSaldo := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select SALDO');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mSaldo := Fields[0].AsFloat;
      end;
      Close;
    end;

    mSaldo := mSaldo - pValor;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('SALDO =' + FloatToStr(mSaldo));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_CONSECUTIVOS'}
procedure TdmObjects.DB_TB_CONSECUTIVOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.FACTURASCLIENTES,');
      SQL.Add('A.ENTRADAS,');
      SQL.Add('A.SALIDAS,');
      SQL.Add('A.PROFORMAS,');
      SQL.Add('A.RECIBOSABONOSCLIENTES,');
      SQL.Add('A.APARTADOS,');
      SQL.Add('A.RECIBOSABONOSAPARTADOS,');
      SQL.Add('A.NOTASCREDITO,');
      SQL.Add('A.NOTASDEBITO,');
      SQL.Add('A.RECIBOSABONOSPROVEEDORES');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CONSECUTIVOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CONSECUTIVOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_CONSECUTIVOS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_CONSECUTIVOS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CONSECUTIVOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CONSECUTIVOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_CONSECUTIVOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_CONSECUTIVOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_EMPRESAS'}
procedure TdmObjects.DB_TB_EMPRESAS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CEDULA,');
      SQL.Add('A.DIRECCION,');
      SQL.Add('A.TELEFONO1,');
      SQL.Add('A.TELEFONO2,');
      SQL.Add('A.FAX,');
      SQL.Add('A.EMAIL,');
      SQL.Add('A.WEB,');
      SQL.Add('A.NOMBREJURIDICO,');
      SQL.Add('A.REDSOCIAL,');
      SQL.Add('A.INICIOPERIODOFISCAL');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_EMPRESAS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_EMPRESAS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_EMPRESAS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_EMPRESAS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_EMPRESAS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_EMPRESAS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_EMPRESAS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_EMPRESAS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_ENTRADASDETALLE'}
procedure TdmObjects.DB_TB_ENTRADASDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CANTIDAD,');
      SQL.Add('A.CODIGOUNIDADMEDIDA,');
      SQL.Add('A.NOMBREUNIDADMEDIDA,');
      SQL.Add('A.PRECIOUNITARIO,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.PRECIOUNITARIOFINAL,');
      SQL.Add('A.PRECIOFINAL,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOIMPUESTO,');
      SQL.Add('A.NOMBREIMPUESTO,');
      SQL.Add('A.IdProducto');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_ENTRADASDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ENTRADASDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ENTRADASDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ENTRADASDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_ENTRADASDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASDETALLE_Sumar_Existencias(qry1: TFDQuery; pWhere: string;
  pTabla: string; d1: TFDMemTable; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mIdProducto: Integer;
  mExistencia: Double;
  mWhere: TStringList;
begin
  try
    pResultado := 1;

    with d1 do
    begin
      First;
      while not Eof do
      begin
        mIdProducto :=
          FieldByName('IdProducto').AsInteger;

        if mIdProducto > 0 then
        begin
          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          mExistencia :=
            DB_TB_PRODUCTOS_Obtener_Existencia(qry1, mWhere.Text,
            pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');

          mExistencia := mExistencia +
            FieldByName('CANTIDAD').AsFloat;

          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          DB_TB_PRODUCTOS_Actualizar_Existencia(qry1, mWhere.Text,
            mExistencia, pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');
        end;

        Next;
      end;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASDETALLE_Restar_Existencias(qry1: TFDQuery; pWhere: string;
  pTabla: string; d1: TFDMemTable; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mIdProducto: Integer;
  mExistencia: Double;
  mWhere: TStringList;
begin
  try
    pResultado := 1;

    with d1 do
    begin
      First;
      while not Eof do
      begin
        mIdProducto :=
          FieldByName('IdProducto').AsInteger;

        if mIdProducto > 0 then
        begin
          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          mExistencia :=
            DB_TB_PRODUCTOS_Obtener_Existencia(qry1, mWhere.Text,
            pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');

          mExistencia := mExistencia -
            FieldByName('CANTIDAD').AsFloat;

          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          DB_TB_PRODUCTOS_Actualizar_Existencia(qry1, mWhere.Text,
            mExistencia, pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');
        end;

        Next;
      end;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_ENTRADASENCABEZADO'}
procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.CODIGOMESA,');
      SQL.Add('A.NUMEROMESA,');
      SQL.Add('A.CODIGOSALONERO,');
      SQL.Add('A.USUARIOSALONERO,');
      SQL.Add('A.NOMBRESALONERO,');
      SQL.Add('A.CODIGOPROVEEDOR,');
      SQL.Add('A.NOMBREPROVEEDOR,');
      SQL.Add('A.SUBTOTAL,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.ISP,');
      SQL.Add('A.ISM,');
      SQL.Add('A.TRANSPORTE,');
      SQL.Add('A.TOTAL,');
      SQL.Add('A.TOTALGRAVADO,');
      SQL.Add('A.TOTALEXCENTO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.SALDO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.NUMEROPROFORMA,');
      SQL.Add('A.NUMEROSPROFORMA,');
      SQL.Add('A.PAGOCON,');
      SQL.Add('A.CAMBIO,');
      SQL.Add('A.IdProveedor,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_ENTRADASENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ENTRADASENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ENTRADASENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ENTRADASENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Sumar_Totales(qry1: TFDQuery; pWhere: string;
  pClaseNombre: string; var pSubTotal: Double;
  var pDescuento: Double; var pTotal: Double;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    pSubTotal := 0;
    pDescuento := 0;
    pTotal := 0;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('SELECT SUM(SUBTOTAL),SUM(DESCUENTOM),SUM(TOTAL)');
      SQL.Add('FROM');
      SQL.Add('(');
      SQL.Add('Select Sum(A.SUBTOTAL) As SUBTOTAL,Sum(A.DESCUENTOM) As DESCUENTOM,Sum(A.TOTAL) As TOTAL');
      SQL.Add('From tb_entradasencabezado A');
      SQL.Add(pWhere);
      SQL.Add('Union');
      SQL.Add('SELECT SUM(A.TOTALFINAL) As SUBTOTAL,Sum(0) As DESCUENTOM,SUM(A.TOTALFINAL) As TOTAL');
      SQL.Add('FROM tb_recibosproveedoresencabezado A');
      SQL.Add(pWhere);
      SQL.Add(') totales');
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          pSubTotal := Fields[0].AsFloat;
        if Fields[1].IsNull = False then
          pDescuento := Fields[1].AsFloat;
        if Fields[2].IsNull = False then
          pTotal := Fields[2].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ENTRADASENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_ESTADOS'}
procedure TdmObjects.DB_TB_ESTADOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ESTADOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ESTADOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_ESTADOS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_ESTADOS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ESTADOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ESTADOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_ESTADOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_ESTADOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_FACTURASCLIENTESDETALLE'}
procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CANTIDAD,');
      SQL.Add('A.CODIGOUNIDADMEDIDA,');
      SQL.Add('A.NOMBREUNIDADMEDIDA,');
      SQL.Add('A.PRECIOUNITARIO,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.PRECIOUNITARIOFINAL,');
      SQL.Add('A.PRECIOFINAL,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOIMPUESTO,');
      SQL.Add('A.NOMBREIMPUESTO,');
      SQL.Add('A.PRECIOMAYORISTAAPLICADO,');
      SQL.Add('A.ISP,');
      SQL.Add('A.ISM,');
      SQL.Add('A.PRECIOUNITARIOBASE,');
      SQL.Add('A.PRECIOFINALBASE,');
      SQL.Add('A.DESCRIPCION1,');
      SQL.Add('A.DESCRIPCION2,');
      SQL.Add('A.IdProducto');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Sumar_Existencias(qry1: TFDQuery; pWhere: string;
  pTabla: string; d1: TFDMemTable; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mIdProducto, EsCompuesto, mGrupoId, mIdProducto2: Integer;
  mExistencia: Double;
  mWhere: TStringList;
  d2: TFDMemTable;
  mTipo: Integer;
begin
  try
    pResultado := 1;

    with d1 do
    begin
      First;
      while not Eof do
      begin
        mIdProducto :=
          FieldByName('IdProducto').AsInteger;

        mWhere := TStringList.Create;
        mWhere.Add('Where Id=' + IntToStr(mIdProducto));
        mTipo :=
          DB_TB_PRODUCTOS_Obtener_Valor(qry1, mWhere.Text,
          'TB_PRODUCTOS', 'IdTipo', pClaseNombre, pResultado, pErrorM);
        mWhere.Free;
        if pResultado = -1 then
          raise Exception.Create('');

        if (mIdProducto > 0) and (mTipo = 1) then
        begin
          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          mExistencia :=
            DB_TB_PRODUCTOS_Obtener_Existencia(qry1, mWhere.Text,
            pClaseNombre, pResultado, pErrorM);
          EsCompuesto := DB_TB_PRODUCTOS_Obtener_Valor(qry1, mWhere.Text,
            'tb_Productos', 'ESCOMPUESTO', pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');

          mExistencia := mExistencia +
            FieldByName('CANTIDAD').AsFloat;

          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          DB_TB_PRODUCTOS_Actualizar_Existencia(qry1, mWhere.Text,
            mExistencia, pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');

          if EsCompuesto = 1 then
          begin
            mWhere := TStringList.Create;
            mWhere.Add('Where IdProducto=' + IntToStr(mIdProducto));
            mGrupoId := DB_TB_GruposEncabezado_Obtener_Id(qry1, mWhere.Text, 'tb_grupos_encabezado', pClaseNombre, pResultado, pErrorM);
            mWhere.Free;

            mWhere := TStringList.Create;
            d2 := TFDMemTable.Create(nil);
            d2.FieldDefs.Clear;
            d2.FieldDefs.Add('ID', ftInteger, 0);
            d2.FieldDefs.Add('IdEncabezado', ftInteger, 0);
            d2.FieldDefs.Add('IdProducto', ftInteger, 0);
            d2.FieldDefs.Add('Cantidad', ftFloat, 0);
            d2.FieldDefs.Add('PrecioFinal', ftFloat, 0);
            d2.FieldDefs.Add('ProductoNombre', ftString, 100);
            d2.CreateDataSet;
            d2.Open;
            mWhere.Add('Where A.IdEncabezado=' + IntToStr(mGrupoId));
            DB_TB_GruposDetalle_Consultar(qry1, d2, mWhere.Text, '', '', '', 'tb_grupos_detalle', '', pResultado, pErrorM);
            mWhere.Free;
            d2.First;
            while not d2.Eof do
            begin
              mIdProducto2 := d2.FieldByName('IdProducto').AsInteger;
              mWhere := TStringList.Create;
              mWhere.Add('Where Id=' + IntToStr(mIdProducto2));
              mExistencia :=
                DB_TB_PRODUCTOS_Obtener_Existencia(qry1, mWhere.Text,
                pClaseNombre, pResultado, pErrorM);
              mWhere.Free;
              if pResultado = -1 then
                raise Exception.Create('');
              mExistencia := mExistencia + d2.FieldByName('CANTIDAD').AsFloat;

              mWhere := TStringList.Create;
              mWhere.Add('Where Id=' + IntToStr(mIdProducto2));
              mTipo :=
                DB_TB_PRODUCTOS_Obtener_Valor(qry1, mWhere.Text,
                'TB_PRODUCTOS', 'IdTipo', pClaseNombre, pResultado, pErrorM);
              mWhere.Free;
              if pResultado = -1 then
                raise Exception.Create('');

              if mTipo = 1 then
              begin
                mWhere := TStringList.Create;
                mWhere.Add('Where Id=' + IntToStr(mIdProducto2));
                DB_TB_PRODUCTOS_Actualizar_Existencia(qry1, mWhere.Text,
                  mExistencia, pClaseNombre, pResultado, pErrorM);
                mWhere.Free;
                if pResultado = -1 then
                  raise Exception.Create('');
              end;

              d2.Next;
            end;
          end;

        end;

        Next;
      end;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESDETALLE_Restar_Existencias(qry1: TFDQuery; pWhere: string;
  pTabla: string; d1: TFDMemTable; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mIdProducto, EsCompuesto, mGrupoId, mIdProducto2: Integer;
  mExistencia: Double;
  mWhere: TStringList;
  d2: TFDMemTable;
  mTipo: Integer;
begin
  try
    pResultado := 1;

    with d1 do
    begin
      First;
      while not Eof do
      begin
        mIdProducto :=
          FieldByName('IdProducto').AsInteger;

        mWhere := TStringList.Create;
        mWhere.Add('Where Id=' + IntToStr(mIdProducto));
        mTipo :=
          DB_TB_PRODUCTOS_Obtener_Valor(qry1, mWhere.Text,
          'TB_PRODUCTOS', 'IdTipo', pClaseNombre, pResultado, pErrorM);
        mWhere.Free;
        if pResultado = -1 then
          raise Exception.Create('');

        if (mIdProducto > 0) and (mTipo = 1) then
        begin
          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          mExistencia :=
            DB_TB_PRODUCTOS_Obtener_Existencia(qry1, mWhere.Text,
            pClaseNombre, pResultado, pErrorM);
          EsCompuesto := DB_TB_PRODUCTOS_Obtener_Valor(qry1, mWhere.Text,
            'tb_Productos', 'ESCOMPUESTO', pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');

          mExistencia := mExistencia -
            FieldByName('CANTIDAD').AsFloat;

          mWhere := TStringList.Create;
          mWhere.Add('Where Id=' + IntToStr(mIdProducto));
          DB_TB_PRODUCTOS_Actualizar_Existencia(qry1, mWhere.Text,
            mExistencia, pClaseNombre, pResultado, pErrorM);
          mWhere.Free;
          if pResultado = -1 then
            raise Exception.Create('');

          if EsCompuesto = 1 then
          begin
            mWhere := TStringList.Create;
            mWhere.Add('Where IdProducto=' + IntToStr(mIdProducto));
            mGrupoId := DB_TB_GruposEncabezado_Obtener_Id(qry1, mWhere.Text, 'tb_grupos_encabezado', pClaseNombre, pResultado, pErrorM);
            mWhere.Free;

            mWhere := TStringList.Create;
            d2 := TFDMemTable.Create(nil);
            d2.FieldDefs.Clear;
            d2.FieldDefs.Add('ID', ftInteger, 0);
            d2.FieldDefs.Add('IdEncabezado', ftInteger, 0);
            d2.FieldDefs.Add('IdProducto', ftInteger, 0);
            d2.FieldDefs.Add('Cantidad', ftFloat, 0);
            d2.FieldDefs.Add('PrecioFinal', ftFloat, 0);
            d2.FieldDefs.Add('ProductoNombre', ftString, 100);
            d2.CreateDataSet;
            d2.Open;
            mWhere.Add('Where A.IdEncabezado=' + IntToStr(mGrupoId));
            DB_TB_GruposDetalle_Consultar(qry1, d2, mWhere.Text, '', '', '', 'tb_grupos_detalle', '', pResultado, pErrorM);
            mWhere.Free;
            d2.First;
            while not d2.Eof do
            begin
              mIdProducto2 := d2.FieldByName('IdProducto').AsInteger;
              mWhere := TStringList.Create;
              mWhere.Add('Where Id=' + IntToStr(mIdProducto2));
              mExistencia :=
                DB_TB_PRODUCTOS_Obtener_Existencia(qry1, mWhere.Text,
                pClaseNombre, pResultado, pErrorM);
              mWhere.Free;
              if pResultado = -1 then
                raise Exception.Create('');
              mExistencia := mExistencia - d2.FieldByName('CANTIDAD').AsFloat;

              mWhere := TStringList.Create;
              mWhere.Add('Where Id=' + IntToStr(mIdProducto2));
              mTipo :=
                DB_TB_PRODUCTOS_Obtener_Valor(qry1, mWhere.Text,
                'TB_PRODUCTOS', 'IdTipo', pClaseNombre, pResultado, pErrorM);
              mWhere.Free;
              if pResultado = -1 then
                raise Exception.Create('');

              if mTipo = 1 then
              begin
                mWhere := TStringList.Create;
                mWhere.Add('Where Id=' + IntToStr(mIdProducto2));
                DB_TB_PRODUCTOS_Actualizar_Existencia(qry1, mWhere.Text,
                  mExistencia, pClaseNombre, pResultado, pErrorM);
                mWhere.Free;
                if pResultado = -1 then
                  raise Exception.Create('');
              end;

              d2.Next;
            end;
          end;

        end;

        Next;
      end;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_Detalles(qry1: TFDQuery; pWhere: string;
  pWhere2: string;
  pTabla: string; pClaseNombre: string; ds1: TFDMemTable;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mIdProducto: Integer;
  mExistencia: Double;
  mWhere: TStringList;
  mNombre: string;
begin
  try
    pResultado := 1;

    mNombre := QuotedStr('Abono');
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select B.NOMBRE As Detalle,B.CANTIDAD As CANTIDAD,A.TOTAL As Total,A.Fecha,A.Numero,1 As Tipo,A.Hora');
      SQL.Add('From TB_FACTURASCLIENTESENCABEZADO A');
      SQL.Add('Left Join TB_FACTURASCLIENTESDETALLE B On A.Numero=B.Numero');
      SQL.Add(pWhere2);
      SQL.Add('Union');
      SQL.Add('Select ' + mNombre + ' As Detalle,0 As CANTIDAD,A.TOTALFINAL As Total,A.Fecha,A.Numero,2 As Tipo,A.Hora');
      SQL.Add('From TB_RECIBOSCLIENTESENCABEZADO A');
      SQL.Add('Left Join TB_RECIBOSCLIENTESDETALLE B On A.Numero=B.Numero');
      SQL.Add(pWhere);
      SQL.Add('Order By 3');
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_FACTURASCLIENTESENCABEZADO'}
procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.CODIGOMESA,');
      SQL.Add('A.NUMEROMESA,');
      SQL.Add('A.CODIGOSALONERO,');
      SQL.Add('A.USUARIOSALONERO,');
      SQL.Add('A.NOMBRESALONERO,');
      SQL.Add('A.CODIGOCLIENTE,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.SUBTOTAL,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.ISP,');
      SQL.Add('A.ISM,');
      SQL.Add('A.TRANSPORTE,');
      SQL.Add('A.TOTAL,');
      SQL.Add('A.TOTALGRAVADO,');
      SQL.Add('A.TOTALEXCENTO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.SALDO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.NUMEROPROFORMA,');
      SQL.Add('A.NUMEROSPROFORMA,');
      SQL.Add('A.PAGOCON,');
      SQL.Add('A.CAMBIO,');
      SQL.Add('A.EFECTIVO,');
      SQL.Add('A.TARJETA,');
      SQL.Add('A.TRANSFERENCIA,');
      SQL.Add('A.CHEQUE,');
      SQL.Add('A.IdCliente,');
      SQL.Add('A.IdVendedor,');
      SQL.Add('B.Nombre As EstadoNombre,');
      SQL.Add('C.DIRECCION As ClienteDireccion,');
      SQL.Add('C.TELEFONO1 As ClienteTelefono1,');
      SQL.Add('C.CONTACTONOMBRE As ClienteContactoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add('Left Join TB_Clientes C on A.IdCliente=C.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Actualizar_Saldo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pValor: Double; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('SALDO = ' + FloatToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_TotalesPago(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pEfectivo: Double; var pTarjeta: Double;
  var pCheque: Double; var pTransferencia: Double;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    pEfectivo := 0;
    pTarjeta := 0;
    pCheque := 0;
    pTransferencia := 0;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('Sum(EFECTIVO),');
      SQL.Add('Sum(TARJETA),');
      SQL.Add('Sum(TRANSFERENCIA),');
      SQL.Add('Sum(CHEQUE)');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          pEfectivo := Fields[0].AsFloat;
        if Fields[1].IsNull = False then
          pTarjeta := Fields[1].AsFloat;
        if Fields[2].IsNull = False then
          pTransferencia := Fields[2].AsFloat;
        if Fields[3].IsNull = False then
          pCheque := Fields[3].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Sumar_Totales(qry1: TFDQuery; pWhere: string;
  pClaseNombre: string; var pSubTotal: Double;
  var pDescuento: Double; var pServicio: Double; var pIVA: Double;var pTotal: Double;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    pSubTotal := 0;
    pDescuento := 0;
    pServicio := 0;
    pIVA := 0;
    pTotal := 0;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('select Sum(SUBTOTAL),Sum(DESCUENTOM),Sum(ISM),Sum(IVAM),Sum(TOTAL)');
      SQL.Add('FROM');
      SQL.Add('(');
      SQL.Add('select Sum(A.SUBTOTAL) As SUBTOTAL,Sum(A.DESCUENTOM) As DESCUENTOM,Sum(A.ISM) As ISM,Sum(A.IVAM) As IVAM,Sum(A.TOTAL) As TOTAL');
      SQL.Add('From TB_FACTURASCLIENTESENCABEZADO A');
      SQL.Add(pWhere);
      SQL.Add('Union all');
      SQL.Add('SELECT SUM(A.TOTALFINAL) As SUBTOTAL,Sum(0) As DESCUENTOM,Sum(0) As ISM, Sum(0) As IVAM,SUM(A.TOTALFINAL) As TOTAL');
      SQL.Add('FROM TB_RECIBOSCLIENTESENCABEZADO A');
      SQL.Add(pWhere);
      SQL.Add('Union all');
      SQL.Add('SELECT SUM(A.TOTALFINAL) As SUBTOTAL,Sum(0) As DESCUENTOM,Sum(0) As ISM, Sum(0) As IVAM,SUM(A.TOTALFINAL) As TOTAL');
      SQL.Add('FROM TB_RECIBOSAPARTADOSENCABEZADO A');
      SQL.Add(pWhere);
      SQL.Add(') totales');
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          pSubTotal := Fields[0].AsFloat;
        if Fields[1].IsNull = False then
          pDescuento := Fields[1].AsFloat;
        if Fields[2].IsNull = False then
          pServicio := Fields[2].AsFloat;
        if Fields[3].IsNull = False then
          pIVA := Fields[3].AsFloat;
        if Fields[4].IsNull = False then
          pTotal := Fields[4].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

function TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Obtener_SaldoFecha(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Double;
var
  mResultado: Double;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Sum(Total)');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_FACTURASCLIENTESENCABEZADO_Estadistico(qry1: TFDQuery; pWhere: string;
  ds1: TFDMemTable; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('first 10 ');
      SQL.Add('B.IDPRODUCTO,');
      SQL.Add('B.Cantidad,');
      SQL.Add('C.Nombre As ProductoNombre,');
      SQL.Add('C.Codigo As ProductoCodigo');
      SQL.Add('from TB_FACTURASCLIENTESENCABEZADO A');
      SQL.Add('Left Join TB_FACTURASCLIENTESDETALLE B On A.Numero=B.NUMERO');
      SQL.Add('Left Join TB_Productos C On B.IDPRODUCTO=C.Id');
      SQL.Add('Where A.CODIGOESTADO = 2');
      SQL.Add('Order By B.Cantidad');
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_FAMILIAS'}
procedure TdmObjects.DB_TB_FAMILIAS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FAMILIAS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FAMILIAS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FAMILIAS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_FAMILIAS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FAMILIAS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FAMILIAS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FAMILIAS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_FAMILIAS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_FORMASPAGO'}
procedure TdmObjects.DB_TB_FORMASPAGO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FORMASPAGO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FORMASPAGO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_FORMASPAGO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_FORMASPAGO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FORMASPAGO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FORMASPAGO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_FORMASPAGO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_FORMASPAGO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_IMPUESTOS'}
procedure TdmObjects.DB_TB_IMPUESTOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.PORCENTAJE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_IMPUESTOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_IMPUESTOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_IMPUESTOS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_IMPUESTOS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_IMPUESTOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_IMPUESTOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_IMPUESTOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_IMPUESTOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

function TdmObjects.DB_TB_IMPUESTOS_Obtener_Porcentaje(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Double;
var
  mResultado: Double;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select PORCENTAJE');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_IMPUESTOS_Obtener_PorcentajeValor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Double;
var
  mResultado, mPorcentaje: Double;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    mPorcentaje := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select PORCENTAJE');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mPorcentaje := Fields[0].AsInteger;
      end;
      Close;
    end;

    if mPorcentaje > 0 then
      mResultado := mPorcentaje / 100;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;
{$ENDREGION}

{$REGION 'TB_NOTASCREDITOENCABEZADO'}
procedure TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.Numero,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.IdCliente,');
      SQL.Add('A.NumeroDocumento,');
      SQL.Add('A.Total,');
      SQL.Add('A.Comentarios,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.EFECTIVO,');
      SQL.Add('A.TARJETA,');
      SQL.Add('A.TRANSFERENCIA,');
      SQL.Add('A.CHEQUE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_NOTASCREDITOENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_NOTASDEBITOENCABEZADO'}
procedure TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.MONTOLETRAS,');
      SQL.Add('A.SALDOANTERIOR,');
      SQL.Add('A.MONTONUMERO,');
      SQL.Add('A.SALDOACTUAL,');
      SQL.Add('A.CODIGOCLIENTE,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.INTERESESP,');
      SQL.Add('A.INTERESESM,');
      SQL.Add('A.TOTALFINAL,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.DESCRIPCION');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_NOTASDEBITOENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_PLANTILLAS'}
procedure TdmObjects.DB_TB_PLANTILLAS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.DESCRIPCION,');
      SQL.Add('A.PLANTILLA');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PLANTILLAS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PLANTILLAS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PLANTILLAS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_PLANTILLAS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PLANTILLAS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PLANTILLAS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PLANTILLAS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_PLANTILLAS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

function TdmObjects.DB_TB_PLANTILLAS_Obtener_Plantilla(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): string;
var
  mResultado: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := '';
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select PLANTILLA');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsString;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;
{$ENDREGION}

{$REGION 'TB_PRODUCTOS'}
procedure TdmObjects.DB_TB_PRODUCTOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CODIGOTIPOBARRAS,');
      SQL.Add('A.DESCRIPCION,');
      SQL.Add('A.CODIGOFAMILIA,');
      SQL.Add('A.CODIGOUNIDADMEDIDA,');
      SQL.Add('A.EXISTENCIA,');
      SQL.Add('A.EXISTENCIAMINIMA,');
      SQL.Add('A.APLICAREXISTENCIAMINIMA,');
      SQL.Add('A.APLICARIVA,');
      SQL.Add('A.CODIGOUBICACION,');
      SQL.Add('A.COMPROMETIDOS,');
      SQL.Add('A.FECHAALTA,');
      SQL.Add('A.PRECIOCOSTO,');
      SQL.Add('A.PRECIOVENTA,');
      SQL.Add('A.IMAGEN1,');
      SQL.Add('A.NOMBREARCHIVO,');
      SQL.Add('A.LISTAVENTAS,');
      SQL.Add('A.LISTACOMPRAS,');
      SQL.Add('A.ESCOMPUESTO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.UTILIDAD,');
      SQL.Add('A.LISTAMENUS,');
      SQL.Add('A.CODIGOIMPUESTO,');
      SQL.Add('A.IMPRIMECOMANDA,');
      SQL.Add('A.CODIGOCOMANDA,');
      SQL.Add('A.RECETA,');
      SQL.Add('A.CODIGOBARRAS,');
      SQL.Add('A.PRECIOVENTASIMPLE,');
      SQL.Add('A.PRECIOMAYORISTA,');
      SQL.Add('A.CODIGOPROVEEDOR,');
      SQL.Add('A.IDProveedor,');
      SQL.Add('A.IdTipo,');
      SQL.Add('A.FechaVencimiento,');
      SQL.Add('B.PORCENTAJE As PORCENTAJEIVA,');
      SQL.Add('C.NOMBRE As FamliaNombre,');
      SQL.Add('D.NOMBRECOMPLETO As ProveedorNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_IMPUESTOS B On A.CODIGOIMPUESTO=B.ID');
      SQL.Add('Left Join TB_FAMILIAS C On A.CODIGOFAMILIA=C.ID');
      SQL.Add('Left Join TB_PROVEEDORES D On A.IDProveedor=D.ID');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PRODUCTOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PRODUCTOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PRODUCTOS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_PRODUCTOS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PRODUCTOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PRODUCTOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PRODUCTOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_PRODUCTOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

function TdmObjects.DB_TB_PRODUCTOS_Obtener_ImpuestoPorcentaje(qry1: TFDQuery;
  pWhere: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Double;
var
  mResultado: Double;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select B.PORCENTAJE As PORCENTAJEIVA');
      SQL.Add('From TB_PRODUCTOS A');
      SQL.Add('Left Join TB_IMPUESTOS B On A.CODIGOIMPUESTO=B.ID');
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PRODUCTOS_Obtener_Existencia(qry1: TFDQuery;
  pWhere: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Double;
var
  mResultado: Double;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select EXISTENCIA');
      SQL.Add('From TB_PRODUCTOS');
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_PRODUCTOS_Actualizar_Existencia(qry1: TFDQuery;
  pWhere: string; pExistencia: Double; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update TB_PRODUCTOS');
      SQL.Add('Set');
      SQL.Add('EXISTENCIA = ' + FloatToStr(pExistencia));
      SQL.Add(pWhere);
      ExecSQL;
    end;

  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

function TdmObjects.DB_TB_PRODUCTOS_Obtener_PrecioMayorista(qry1: TFDQuery;
  pWhere: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Double;
var
  mResultado: Double;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select PRECIOMAYORISTA');
      SQL.Add('From TB_PRODUCTOS');
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PRODUCTOS_Obtener_Nombre(qry1: TFDQuery;
  pWhere: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): string;
var
  mResultado: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := '';
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Nombre');
      SQL.Add('From TB_PRODUCTOS');
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsString;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;
{$ENDREGION}

{$REGION 'TB_PROFORMASDETALLE'}
procedure TdmObjects.DB_TB_PROFORMASDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CANTIDAD,');
      SQL.Add('A.CODIGOUNIDADMEDIDA,');
      SQL.Add('A.NOMBREUNIDADMEDIDA,');
      SQL.Add('A.PRECIOUNITARIO,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.PRECIOUNITARIOFINAL,');
      SQL.Add('A.PRECIOFINAL,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOIMPUESTO,');
      SQL.Add('A.NOMBREIMPUESTO,');
      SQL.Add('A.PRECIOMAYORISTAAPLICADO,');
      SQL.Add('A.IdProducto');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_PROFORMASDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROFORMASDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROFORMASDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROFORMASDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_PROFORMASDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_PROFORMASENCABEZADO'}
procedure TdmObjects.DB_TB_PROFORMASENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.CODIGOMESA,');
      SQL.Add('A.NUMEROMESA,');
      SQL.Add('A.CODIGOSALONERO,');
      SQL.Add('A.USUARIOSALONERO,');
      SQL.Add('A.NOMBRESALONERO,');
      SQL.Add('A.CODIGOCLIENTE,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.SUBTOTAL,');
      SQL.Add('A.DESCUENTOP,');
      SQL.Add('A.DESCUENTOM,');
      SQL.Add('A.IVAP,');
      SQL.Add('A.IVAM,');
      SQL.Add('A.ISP,');
      SQL.Add('A.ISM,');
      SQL.Add('A.TRANSPORTE,');
      SQL.Add('A.TOTAL,');
      SQL.Add('A.TOTALGRAVADO,');
      SQL.Add('A.TOTALEXCENTO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.SALDO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.NUMEROFACTURA,');
      SQL.Add('A.NUMEROSFACTURA,');
      SQL.Add('A.PAGOCON,');
      SQL.Add('A.CAMBIO,');
      SQL.Add('A.IdCliente,');
      SQL.Add('A.IdVendedor,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_PROFORMASENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROFORMASENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROFORMASENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROFORMASENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_PROFORMASENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROFORMASENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_PROVEEDORES'}
procedure TdmObjects.DB_TB_PROVEEDORES_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.FECHAALTA,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.APELLIDO1,');
      SQL.Add('A.APELLIDO2,');
      SQL.Add('A.NOMBRECOMPLETO,');
      SQL.Add('A.CODIGOPOSTAL,');
      SQL.Add('A.DIRECCION,');
      SQL.Add('A.EMAIL,');
      SQL.Add('A.TELEFONO1,');
      SQL.Add('A.TELEFONO2,');
      SQL.Add('A.WEB,');
      SQL.Add('A.NOMBREIMPRIMIR,');
      SQL.Add('A.CEDULA,');
      SQL.Add('A.IMAGEN1,');
      SQL.Add('A.NOMBREARCHIVO,');
      SQL.Add('A.FECHANACIMIENTO,');
      SQL.Add('A.SALDO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROVEEDORES_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROVEEDORES_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROVEEDORES_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_PROVEEDORES_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROVEEDORES_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROVEEDORES_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_PROVEEDORES_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_PROVEEDORES_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROVEEDORES_Sumar_Saldo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pValor: Double; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mSaldo: Double;
begin
  try
    pResultado := 1;

    mSaldo := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select SALDO');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mSaldo := Fields[0].AsFloat;
      end;
      Close;
    end;

    mSaldo := mSaldo + pValor;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('SALDO =' + FloatToStr(mSaldo));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_PROVEEDORES_Restar_Saldo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pValor: Double; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
  mSaldo: Double;
begin
  try
    pResultado := 1;

    mSaldo := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select SALDO');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mSaldo := Fields[0].AsFloat;
      end;
      Close;
    end;

    mSaldo := mSaldo - pValor;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('SALDO =' + FloatToStr(mSaldo));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_RECIBOSAPARTADOSDETALLE'}
procedure TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROFACTURA,');
      SQL.Add('A.NUMEROSFACTURA,');
      SQL.Add('A.MONTOABONO,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_RECIBOSAPARTADOSENCABEZADO'}
procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.MONTOLETRAS,');
      SQL.Add('A.SALDOANTERIOR,');
      SQL.Add('A.MONTONUMERO,');
      SQL.Add('A.SALDOACTUAL,');
      SQL.Add('A.CODIGOCLIENTE,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.INTERESESP,');
      SQL.Add('A.INTERESESM,');
      SQL.Add('A.TOTALFINAL,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.IdCliente,');
      SQL.Add('A.IdVendedor,');
      SQL.Add('A.EFECTIVO,');
      SQL.Add('A.TARJETA,');
      SQL.Add('A.TRANSFERENCIA,');
      SQL.Add('A.CHEQUE,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Obtener_TotalesPago(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pEfectivo: Double; var pTarjeta: Double;
  var pCheque: Double; var pTransferencia: Double;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    pEfectivo := 0;
    pTarjeta := 0;
    pCheque := 0;
    pTransferencia := 0;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('Sum(EFECTIVO),');
      SQL.Add('Sum(TARJETA),');
      SQL.Add('Sum(TRANSFERENCIA),');
      SQL.Add('Sum(CHEQUE)');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          pEfectivo := Fields[0].AsFloat;
        if Fields[1].IsNull = False then
          pTarjeta := Fields[1].AsFloat;
        if Fields[2].IsNull = False then
          pTransferencia := Fields[2].AsFloat;
        if Fields[3].IsNull = False then
          pCheque := Fields[3].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSAPARTADOSENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_RECIBOSCLIENTESDETALLE'}
procedure TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROFACTURA,');
      SQL.Add('A.NUMEROSFACTURA,');
      SQL.Add('A.MONTOABONO,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_RECIBOSCLIENTESENCABEZADO'}
procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.MONTOLETRAS,');
      SQL.Add('A.SALDOANTERIOR,');
      SQL.Add('A.MONTONUMERO,');
      SQL.Add('A.SALDOACTUAL,');
      SQL.Add('A.CODIGOCLIENTE,');
      SQL.Add('A.NOMBRECLIENTE,');
      SQL.Add('A.INTERESESP,');
      SQL.Add('A.INTERESESM,');
      SQL.Add('A.TOTALFINAL,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.IdCliente,');
      SQL.Add('A.IdVendedor,');
      SQL.Add('A.EFECTIVO,');
      SQL.Add('A.TARJETA,');
      SQL.Add('A.TRANSFERENCIA,');
      SQL.Add('A.CHEQUE,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSCLIENTESENCABEZADO_Obtener_TotalesPago(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pEfectivo: Double; var pTarjeta: Double;
  var pCheque: Double; var pTransferencia: Double;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    pEfectivo := 0;
    pTarjeta := 0;
    pCheque := 0;
    pTransferencia := 0;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('Sum(EFECTIVO),');
      SQL.Add('Sum(TARJETA),');
      SQL.Add('Sum(TRANSFERENCIA),');
      SQL.Add('Sum(CHEQUE)');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          pEfectivo := Fields[0].AsFloat;
        if Fields[1].IsNull = False then
          pTarjeta := Fields[1].AsFloat;
        if Fields[2].IsNull = False then
          pTransferencia := Fields[2].AsFloat;
        if Fields[3].IsNull = False then
          pCheque := Fields[3].AsFloat;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_RECIBOSPROVEEDORESDETALLE'}
procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROFACTURA,');
      SQL.Add('A.NUMEROSFACTURA,');
      SQL.Add('A.MONTOABONO,');
      SQL.Add('A.LINEA,');
      SQL.Add('A.NOMBREESTADO,');
      SQL.Add('A.CODIGOESTADO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESDETALLE_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_RECIBOSPROVEEDORESENCABEZADO'}
procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.NUMEROS,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.MONTOLETRAS,');
      SQL.Add('A.SALDOANTERIOR,');
      SQL.Add('A.MONTONUMERO,');
      SQL.Add('A.SALDOACTUAL,');
      SQL.Add('A.CODIGOPROVEEDOR,');
      SQL.Add('A.NOMBREPROVEEDOR,');
      SQL.Add('A.INTERESESP,');
      SQL.Add('A.INTERESESM,');
      SQL.Add('A.TOTALFINAL,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOFORMAPAGO,');
      SQL.Add('A.CODIGOMONEDA,');
      SQL.Add('A.TIPOCAMBIO,');
      SQL.Add('A.SERIE,');
      SQL.Add('A.CODIGOIMPRESA,');
      SQL.Add('A.NUMERODOC,');
      SQL.Add('A.FECHAVENCIMIENTO,');
      SQL.Add('A.CODIGOTIPO,');
      SQL.Add('A.CARCREDITO,');
      SQL.Add('A.CARCONTADO,');
      SQL.Add('A.CODIGOVENDEDOR,');
      SQL.Add('A.NOMBREVENDEDOR,');
      SQL.Add('A.IdProveedor,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B on A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_RECIBOSPROVEEDORESENCABEZADO_Actualizar_Estado(qry1: TFDQuery; pWhere: string;
  pTabla: string; pvalor: Integer; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add('CODIGOESTADO = ' + IntToStr(pvalor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_SISTEMA'}
procedure TdmObjects.DB_TB_SISTEMA_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.VERSIONS,');
      SQL.Add('A.IMAGENFONDO,');
      SQL.Add('A.NOMBREIMAGEN,');
      SQL.Add('A.IVADEFAULT,');
      SQL.Add('A.ISERVICIO,');
      SQL.Add('A.MONEDADEFAULT,');
      SQL.Add('A.FAMILIADEFAULT,');
      SQL.Add('A.UNIDADMEDIDADEFAULT,');
      SQL.Add('A.MINIMODEFAULT,');
      SQL.Add('A.APLICARMINIMODEFAULT,');
      SQL.Add('A.APLICARREDONDEO,');
      SQL.Add('A.APLICARREDONDEO5,');
      SQL.Add('A.Clave,');
      SQL.Add('A.MontoInicial,');
      SQL.Add('A.PRODUCTOIMPUESTOINCLUIDO,');
      SQL.Add('A.USARSEGURIDAD,');
      SQL.Add('A.USARCONTABILIDAD');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_SISTEMA_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_SISTEMA_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_SISTEMA_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_SISTEMA_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_SISTEMA_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_SISTEMA_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_SISTEMA_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_SISTEMA_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_TIPOS'}
procedure TdmObjects.DB_TB_TIPOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_TIPOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_TIPOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_TIPOS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_TIPOS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_TIPOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_TIPOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_TIPOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_TIPOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_UNIDADESMEDIDA'}
procedure TdmObjects.DB_TB_UNIDADESMEDIDA_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.ABREVIATURA');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_UNIDADESMEDIDA_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_UNIDADESMEDIDA_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_UNIDADESMEDIDA_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_UNIDADESMEDIDA_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_UNIDADESMEDIDA_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_UNIDADESMEDIDA_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_UNIDADESMEDIDA_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_UNIDADESMEDIDA_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_USUARIOS'}
procedure TdmObjects.DB_TB_USUARIOS_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.USUARIO,');
      SQL.Add('A.CLAVE,');
      SQL.Add('A.FECHAALTA,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.APELLIDO1,');
      SQL.Add('A.APELLIDO2,');
      SQL.Add('A.NOMBRECOMPLETO,');
      SQL.Add('A.CODIGOPOSTAL,');
      SQL.Add('A.DIRECCION,');
      SQL.Add('A.EMAIL,');
      SQL.Add('A.TELEFONO1,');
      SQL.Add('A.TELEFONO2,');
      SQL.Add('A.CODIGONIVEL,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CEDULA,');
      SQL.Add('A.ESVISIBLE,');
      SQL.Add('A.FECHANACIMIENTO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_USUARIOS_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_USUARIOS_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_USUARIOS_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_USUARIOS_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_USUARIOS_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_USUARIOS_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_USUARIOS_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_USUARIOS_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_VENDEDORES'}
procedure TdmObjects.DB_TB_VENDEDORES_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.FECHAALTA,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.APELLIDO1,');
      SQL.Add('A.APELLIDO2,');
      SQL.Add('A.NOMBRECOMPLETO,');
      SQL.Add('A.CODIGOPOSTAL,');
      SQL.Add('A.DIRECCION,');
      SQL.Add('A.EMAIL,');
      SQL.Add('A.TELEFONO1,');
      SQL.Add('A.TELEFONO2,');
      SQL.Add('A.WEB,');
      SQL.Add('A.NOMBREIMPRIMIR,');
      SQL.Add('A.CEDULA,');
      SQL.Add('A.IMAGEN1,');
      SQL.Add('A.NOMBREARCHIVO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_VENDEDORES_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_VENDEDORES_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_VENDEDORES_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_VENDEDORES_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_VENDEDORES_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_VENDEDORES_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_VENDEDORES_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_VENDEDORES_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'tb_grupos_encabezado'}
procedure TdmObjects.DB_TB_GruposEncabezado_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.Nombre,');
      SQL.Add('A.TotalVenta,');
      SQL.Add('A.TotalCompra,');
      SQL.Add('A.IdProducto');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_GruposEncabezado_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_GruposEncabezado_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_GruposEncabezado_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_GruposEncabezado_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_GruposEncabezado_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_GruposEncabezado_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_GruposEncabezado_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_GruposEncabezado_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'tb_grupos_detalle'}
procedure TdmObjects.DB_TB_GruposDetalle_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.IdEncabezado,');
      SQL.Add('A.IdProducto,');
      SQL.Add('A.Cantidad');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_GruposDetalle_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_GruposDetalle_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_GruposDetalle_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_GruposDetalle_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_GruposDetalle_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_GruposDetalle_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_GruposDetalle_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_GruposDetalle_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'Ordenes Encabezado'}
procedure TdmObjects.DB_TB_OrdenesEncabezado_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.ETIQUETA,');
      SQL.Add('A.VENTA,');
      SQL.Add('A.COMENTARIOS,');
      SQL.Add('A.IDCLIENTE,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.HORA,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('B.NOMBRE As EstadoNombre,');
      SQL.Add('C.NOMBRECOMPLETO As ClienteNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOSORDENES B On A.CODIGOESTADO=B.Id');
      SQL.Add('Left Join TB_CLIENTES C On A.IDCLIENTE=C.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_OrdenesEncabezado_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_OrdenesEncabezado_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_OrdenesEncabezado_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_OrdenesEncabezado_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_OrdenesEncabezado_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_OrdenesEncabezado_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_OrdenesEncabezado_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_OrdenesEncabezado_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'Ordenes Detalle'}
procedure TdmObjects.DB_TB_OrdenesDetalle_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.IdEncabezado,');
      SQL.Add('A.IdProducto,');
      SQL.Add('A.Cantidad,');
      SQL.Add('A.idImpresora,');
      SQL.Add('A.Venta,');
      SQL.Add('B.Nombre As ProductoNombre,');
      SQL.Add('B.Codigo As ProductoCodigo,');
      SQL.Add('B.PRECIOVENTA As PrecioUnitario');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join tb_Productos B On A.IdProducto=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_OrdenesDetalle_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_OrdenesDetalle_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_OrdenesDetalle_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_OrdenesDetalle_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_OrdenesDetalle_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_OrdenesDetalle_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_OrdenesDetalle_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_OrdenesDetalle_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_Licencias'}
procedure TdmObjects.DB_TB_Licencias_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.IdSistema,');
      SQL.Add('A.Sistema,');
      SQL.Add('A.Licencia,');
      SQL.Add('A.CodigoEstado,');
      SQL.Add('A.CodigoVenta');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_Licencias_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_Licencias_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_Licencias_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_Licencias_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_Licencias_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_Licencias_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_Licencias_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_Licencias_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_Cuentas'}
procedure TdmObjects.DB_TB_Cuentas_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE,');
      SQL.Add('A.CODIGO,');
      SQL.Add('A.IDTIPO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOPADREID,');
      SQL.Add('A.NIVEL,');
      SQL.Add('A.CODIGOPADRENOMBRE,');
      SQL.Add('A.TEMP,');
      SQL.Add('A.SALDO,');
      SQL.Add('A.Editable,');
      SQL.Add('A.MOSTRALISTA,');
      SQL.Add('B.Nombre As EstadoNombre,');
      SQL.Add('C.Nombre As TipoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B On A.CODIGOESTADO=B.Id');
      SQL.Add('Left Join TB_TIPOSCUENTAS C On A.IDTIPO=C.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_Cuentas_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_Cuentas_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_Cuentas_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_Cuentas_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_Cuentas_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_Cuentas_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_Cuentas_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_Cuentas_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_AsientosEncabezado'}
procedure TdmObjects.DB_TB_AsientosEncabezado_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.FECHACREACION,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.NUMERO,');
      SQL.Add('A.CODIGOESTADO,');
      SQL.Add('A.CODIGOCIERRE,');
      SQL.Add('A.TOTAL,');
      SQL.Add('B.Nombre As EstadoNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_ESTADOS B On A.CODIGOESTADO=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_AsientosEncabezado_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_AsientosEncabezado_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_AsientosEncabezado_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_AsientosEncabezado_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_AsientosEncabezado_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_AsientosEncabezado_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_AsientosEncabezado_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_AsientosEncabezado_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_AsientosDetalle'}
procedure TdmObjects.DB_TB_AsientosDetalle_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.IDENCABEZADO,');
      SQL.Add('A.IDMOVIMIENTO,');
      SQL.Add('A.IDCUENTA,');
      SQL.Add('A.DESCRIPCION,');
      SQL.Add('A.REFERENCIA,');
      SQL.Add('A.MONTO,');
      SQL.Add('A.DEBE,');
      SQL.Add('A.HABER,');
      SQL.Add('A.LINEA,');
      SQL.Add('B.Nombre As CuentaNombre');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add('Left Join TB_Cuentas B On A.IDCUENTA=B.Id');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_AsientosDetalle_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_AsientosDetalle_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_AsientosDetalle_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_AsientosDetalle_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_AsientosDetalle_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_AsientosDetalle_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_AsientosDetalle_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_AsientosDetalle_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_Niveles Usuarios'}
procedure TdmObjects.DB_TB_NivelesUsuarios_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.NOMBRE');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NivelesUsuarios_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NivelesUsuarios_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_NivelesUsuarios_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_NivelesUsuarios_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NivelesUsuarios_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NivelesUsuarios_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_NivelesUsuarios_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_NivelesUsuarios_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

{$REGION 'TB_Movimientos Inventario'}
procedure TdmObjects.DB_TB_MovimientoInventario_Consultar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pOrderBy: string; pGroupBy: string; pHaving: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mSQL: string;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    ds1.EmptyDataSet;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select ');
      SQL.Add('A.ID,');
      SQL.Add('A.FECHA,');
      SQL.Add('A.IDPRODUCTO,');
      SQL.Add('A.CANTIDAD,');
      SQL.Add('A.TIPO,');
      SQL.Add('A.MOTIVO');
      SQL.Add('From ' + pTabla + ' A');
      SQL.Add(pWhere);
      SQL.Add(pGroupBy);
      SQL.Add(pHaving);
      SQL.Add(pOrderBy);
      mSQL := SQL.Text;
      DB_DBToStruct(qry1, ds1, pResultado, pErrorM);
      if pResultado = -1 then
        raise Exception.Create('');
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(mSQL);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Consultar.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;

procedure TdmObjects.DB_TB_MovimientoInventario_Insertar(qry1: TFDQuery; ds1: TFDMemTable;
  pClaseNombre: string; var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;
    DB_Insert_Generico(qry1, ds1, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Insertar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_MovimientoInventario_Modificar(qry1: TFDQuery; ds1: TFDMemTable;
  pWhere: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Update_Generico(qry1, ds1, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Modificar.sql');
        mArchivo.Free;
    end;
  end;
end;

procedure TdmObjects.DB_TB_MovimientoInventario_Eliminar(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    DB_Delete_Generico(qry1, pTabla, pWhere, pResultado, pErrorM);
    if pResultado = -1 then
      raise Exception.Create('');

    pResultado := 1;
  except
    pResultado := -1;
    if SaveLog = 1 then
    begin
      mArchivo := TStringList.Create;
      mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Eliminar.sql');
        mArchivo.Free;
    end;
  end;
end;

function TdmObjects.DB_TB_MovimientoInventario_Existe_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From ' + pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_MovimientoInventario_Obtener_Id(qry1: TFDQuery; pWhere: string;
  pTabla: string; pClaseNombre: string; var pResultado: Integer;
  var pErrorM: string): Integer;
var
  mResultado: Integer;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := 0;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select Id');
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsInteger;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Id.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_MovimientoInventario_Existe_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Boolean;
var
  mResultado: Boolean;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    mResultado := False;
    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := True;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Existe_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

function TdmObjects.DB_TB_MovimientoInventario_Obtener_Valor(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string): Variant;
var
  mResultado: Variant;
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Select '+ pCampo);
      SQL.Add('From '+ pTabla);
      SQL.Add(pWhere);
      Open();
      if IsEmpty = False then
      begin
        if Fields[0].IsNull = False then
          mResultado := Fields[0].AsVariant;
      end;
      Close;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Obtener_Valor.sql');
        mArchivo.Free;
      end;
    end;
  end;

  Result := mResultado;
end;

procedure TdmObjects.DB_TB_MovimientoInventario_Actualizar_Campo(qry1: TFDQuery; pWhere: string;
  pTabla: string; pCampo: string; pValor: Variant; pClaseNombre: string;
  var pResultado: Integer; var pErrorM: string);
var
  mArchivo: TStringList;
begin
  try
    pResultado := 1;

    with qry1 do
    begin
      SQL.Clear;
      SQL.Add('Update ' + pTabla);
      SQL.Add('Set');
      SQL.Add(pCampo + '=' + VarToStr(pValor));
      SQL.Add(pWhere);
      ExecSQL;
    end;

    pResultado := 1;
  except
    on E: Exception do
    begin
      pErrorM := E.Message;
      pResultado := -1;
      if SaveLog = 1 then
      begin
        mArchivo := TStringList.Create;
        mArchivo.Add(qry1.SQL.Text);
        mArchivo.SaveToFile('SQL_' + pClaseNombre +'_Actualizar_Campo.sql');
        mArchivo.Free;
      end;
    end;
  end;
end;
{$ENDREGION}

end.
